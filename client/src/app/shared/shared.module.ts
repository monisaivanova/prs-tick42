import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PaginationComponent } from '../components/pagination/pagination.component';
import { BreadcrumbsSearchComponent } from '../components/breadcrumbs-search/breadcrumbs-search.component';
import { SearchBoxComponent } from '../components/search-box/search-box.component';


@NgModule({
  declarations: [
    PaginationComponent,
    BreadcrumbsSearchComponent,
    //CreateRevReqButtonComponent,
    SearchBoxComponent,
    BreadcrumbsSearchComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,    
  ],
  exports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    BreadcrumbsSearchComponent,
    SearchBoxComponent,
  ],
})
export class SharedModule { }
