import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserInterface } from '../common/interfaces/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {
  private usersSubcription: Subscription;
  public users: UserInterface[] = [];

  constructor(
      private readonly activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.usersSubcription = this.activatedRoute
      .data
      .subscribe(data => {
        this.users = data.users;
      });
  }

  ngOnDestroy() {
    this.usersSubcription.unsubscribe();
  }
}
