import { Component, Input, OnInit } from '@angular/core';
import { RevReqInterface } from '../../common/interfaces/show-rev-req';
import { WorkItemInterface } from '../../common/interfaces/work-item';
import { UsersDataService } from '../services/users-data.service';

@Component({
    selector: 'app-rev-view',
    templateUrl: './rev-view.component.html',
    styleUrls: ['./rev-view.component.css']
  })
  export class RevViewComponent implements OnInit{
  
    @Input() public revReq: RevReqInterface; //object; // todo
    public revReqId : string;
    @Input() public workItem: WorkItemInterface; //WorkItemInterface;
    public workItemId : string;
    @Input() public elementInRevView;
    //public user: any;
    
    constructor(private readonly usersDataService: UsersDataService, ) { }

    ngOnInit(): void {  
      this.revReqId = this.revReq.revReqId; 
      // this.elementInRevView = document.getElementById('statusTypeColour').innerHTML;     

      this.usersDataService.findWorkItemByRevReqId(this.revReqId)
        .subscribe( 
          (workItemMatch) => {
            this.workItem = workItemMatch;
            this.workItemId = this.workItem.workItemId;

      });
      console.log('*************************')
      //console.log(document.getElementById("statusTypeColour").innerHTML)
      console.log(this.elementInRevView)
      //const allViews = document.getElementsByClassName("ribbon-front");
      // {  // works only for 1st selection.... 
      //     if (document.getElementById("statusTypeColour").innerHTML == "Rejected") {
      //         document.getElementById("statusTypeColour").style.backgroundColor = "red";
      //     } else if (document.getElementById("statusTypeColour").innerHTML == "Review Requested") {
      //         document.getElementById("statusTypeColour").style.backgroundColor = "yellow";
      //         document.getElementById("statusTypeColour").style.color = "black" ;
      //     }
      //     else if (this.elementInRevView === "Under Review") {
      //       document.getElementById("statusTypeColour").style.backgroundColor = "orange";
      //       //document.getElementById("statusTypeColour").style.fontStyle = "bold" ;
      //       //document.getElementById("statusTypeColour").style.color = "black" ;
      //     }
      //     else if (this.elementInRevView === "Approved") {
      //       document.getElementById("statusTypeColour").style.backgroundColor = "#45db00d2";
      //       //document.getElementById("statusTypeColour").style.fontStyle = "bold" ;
      //       //document.getElementById("statusTypeColour").style.color = "black" ;
      //     }
      // }
      // copyyyyyyyyyyyyyyyy

      //document.getElementsByClassName("ribbon-front").
      
        // if (this.revReq.status.type === "Rejected") {
        //     document.getElementById("statusTypeColour").style.backgroundColor = "red";
        // } else if (this.revReq.status.type === "Review Requested") {
        //     document.getElementById("statusTypeColour").style.backgroundColor = "yellow";
        //     document.getElementById("statusTypeColour").style.color = "black" ;
        // }
        // else if (this.revReq.status.type === "Under Review") {
        //   document.getElementById("statusTypeColour").style.backgroundColor = "orange";
        //   //document.getElementById("statusTypeColour").style.fontStyle = "bold" ;
        //   //document.getElementById("statusTypeColour").style.color = "black" ;
        // }
        // else if (this.revReq.status.type === "Approved") {
        //   document.getElementById("statusTypeColour").style.backgroundColor = "#45db00d2";
        //   //document.getElementById("statusTypeColour").style.fontStyle = "bold" ;
        //   //document.getElementById("statusTypeColour").style.color = "black" ;
        // }
    
    
      
    }

  }
