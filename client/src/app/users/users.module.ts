import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
// import { WorkItemRoutingModule } from '../work-item/work-item-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { UsersViewComponent } from './user-view/users-view.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ReviewRequestsModule } from '../review-requests/review-requests.module';
import { TeamsViewComponent } from './teams-view/teams-view.component';
import { RevViewComponent } from './rev-view/rev-view.component';
import { TeamsComponent } from './teams/teams.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
// import { ReviewRequestsModule } from '../review-requests/rev-req.module';

@NgModule({
  declarations: [
    UsersComponent,
    UsersViewComponent,
    RevViewComponent,
    TeamsViewComponent,
    TeamDetailsComponent,
    TeamsComponent,   
    UserProfileComponent,
  ],
  imports: [
    SharedModule,
    RouterModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ReviewRequestsModule
    //WorkItemRoutingModule
  ],
  exports: [
     UsersComponent
  ],
})
export class UsersModule { }
