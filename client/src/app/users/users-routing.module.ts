import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users.component';
import { UserResolverService } from './services/user-resolver.service';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserProfileResolverService } from './services/user-proflie-resolver.service';
import { AuthGuard } from '../auth/auth.guard';
import { TeamsComponent } from './teams/teams.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { TeamDetailsResolverService } from './services/team-details-resolver.service';

const routes: Routes = [
    {
      path: '', component: UsersComponent, 
      resolve: {users: UserResolverService}, canActivate: [AuthGuard], pathMatch: 'full' 
    },
    {
      path: ':userId', component: UserProfileComponent, 
      resolve: { user: UserProfileResolverService }, canActivate: [AuthGuard],
    },
    { //target "My Teams" from header ['/users', this.userId, 'teams']
      path: ':userId/teams', component: TeamsComponent, 
      resolve: { user: UserProfileResolverService }, canActivate: [AuthGuard],
    },
    { 
      path: ':userId/teams/:teamId', component: TeamDetailsComponent, 
      resolve: { user: UserProfileResolverService , team: TeamDetailsResolverService}, canActivate: [AuthGuard],
    },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {

}
