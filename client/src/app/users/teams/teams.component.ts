import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserInterface } from '../../common/interfaces/user';
import { UsersDataService } from '../services/users-data.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../app/core/services/auth.service';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { RevReqCommentsBaseComponent } from '../../review-requests/rev-req-comments--base.component';
import { RevReqDataService } from '../../review-requests/services/rev-req-data.service';
import { TeamInterface } from '../../../app/common/interfaces/show-team';
import { StorageService } from '../../core/services/storage.service';
import { RevReqInterface } from '../../common/interfaces/show-rev-req';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-teams',
    templateUrl: './teams.component.html',
    styleUrls: ['./teams.component.css']
  })
  export class TeamsComponent extends RevReqCommentsBaseComponent implements OnInit, OnDestroy {
   public teamInvitations: any[] = [];


    public user: any; //UserInterface;
    public loggedInUserSubscription: Subscription;
    public loggedInUser: string;
    public loggedInUserId: string;
    public teams: TeamInterface[] = []; // TeamDisplayInterface
    public currentJustify = 'justified';
    public isFried: boolean = false;
    public ownRevReqs: RevReqInterface[] = [];
    public revReqs: RevReqInterface[] = [];

    public createTeamForm: FormGroup;
    public newTeam: TeamInterface;
    
    constructor(
      notificator: NotificatorService,
      revReqDataService: RevReqDataService,
      private readonly activatedRoute: ActivatedRoute,
      private readonly usersDataService: UsersDataService,
      private readonly authService: AuthService,
      private readonly storageService: StorageService,
      private readonly formBuilder: FormBuilder,
      private readonly router: Router,


    ) {
      super(revReqDataService, notificator);
    }
  
    ngOnInit(): void {
      this.createTeamForm = this.formBuilder.group({
        teamName: ['', [Validators.required, Validators.minLength(3)]],
      });
      //this.user = this.storageService.get('username')
      this.activatedRoute.data.subscribe((data) => {
  
        //console.log(data);
         this.user = data.user;
          //this.teams = data.teams;
         // this.teams = this.user.teams;
  

  
  
          this.usersDataService.revReqsOfUser(this.user.userId) // own rev reqs
            .subscribe(
              (ownRevReqs) => {
              this.ownRevReqs = ownRevReqs;
          })  
  
          this.usersDataService.revReqsToReviewByUser(this.user.userId) // own rev reqs
          .subscribe(
            (revReqs) => {
            this.revReqs = revReqs;
        })  
          // this.usersDataService.commentsOfUser(this.user.userId)
          // .subscribe(
          //   (comments) => {
          //     this.comments = comments;
          //   }
          // );
  
          this.usersDataService.teamsUserIsMemberOf(
             this.user.userId
             //data.user.userId
            //'e49c0e80-bc00-4741-9ba6-0ac6cda01e14'
            )
            .subscribe(
              // on success // data
              (teams : TeamInterface[]) => { //>?? change made
                this.teams = teams; 
                // this.user.teams = teams;
                //console.log(teams[0]);
              },
              (error) => {console.log(error)},
              // message 3rd callback
              () => {console.log('3rd callback of teams success')},
          );
  
          // // this.usersDataService.teamsUserIsOwnerOf(this.user.userId)
          // //   .subscribe(
          // //     (teams) => {
          // //       //this.teams = teams;
          // //       this.user.createdTeams = teams;
          // //     }
          // // );
  
      }); // activatedRoute closing '}'
    } // ngOnInit closing '}'
  
    public createTeam(): void {
      //console.log(this.createRevReqForm.value);
      this.usersDataService.createTeam(this.createTeamForm.value, this.user).subscribe(
        (team: TeamInterface) => {
          this.newTeam = team;
          this.router.navigate([`users/${this.user.userId}/teams/${this.newTeam.teamId}`]);
          // to clear input firld: https://www.danvega.dev/blog/2017/06/07/angular-forms-clear-input-field/
          this.notificator.success('Team magically created!');
        },
        (error) => {
          this.notificator.error('Team creation unsuccessful!');
        }
      );
    }


    ngOnDestroy() {
      //this.loggedInUserSubscription.unsubscribe();
    }
  
  
  }
  
  