import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserInterface } from '../../common/interfaces/user';
import { TeamInterface } from '../../common/interfaces/show-team';
import { UsersDataService } from '../services/users-data.service';

@Component({
    selector: 'app-teams-view',
    templateUrl: './teams-view.component.html',
    styleUrls: ['./teams-view.component.css']
  })
  export class TeamsViewComponent {
  
    @Input() 
    public team: TeamInterface;

    constructor(

    ) {
        
     }


  }
