import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserInterface } from '../../common/interfaces/user';
import { UsersDataService } from '../services/users-data.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../app/core/services/auth.service';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { RevReqCommentsBaseComponent } from '../../review-requests/rev-req-comments--base.component';
import { RevReqDataService } from '../../review-requests/services/rev-req-data.service';
import { TeamInterface } from '../../../app/common/interfaces/show-team';
import { StorageService } from '../../core/services/storage.service';
import { RevReqInterface } from '../../common/interfaces/show-rev-req';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

// MAJOR 2do - deff not ready // todo
export class UserProfileComponent extends RevReqCommentsBaseComponent implements OnInit, OnDestroy {
  
  public user: any; //UserInterface;
  public loggedInUserSubscription: Subscription;
  public loggedInUser: string;
  public loggedInUserId: string;
  public teams: object[] = []; // TeamDisplayInterface
  public currentJustify = 'justified';
  public isFried: boolean = false;
  public ownRevReqs: RevReqInterface[] = [];
  public revReqs: RevReqInterface[] = [];
  //@Output()   public elementInRevView;
  //@Input() public revReq: object;
  //public showAddRemoveTeamsButtons: boolean = true;

  constructor(
    notificator: NotificatorService,
    revReqDataService: RevReqDataService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly usersDataService: UsersDataService,
    private readonly authService: AuthService,
    private readonly storageService: StorageService,
  ) {
    super(revReqDataService, notificator);
  }

  ngOnInit(): void {

    //this.user = this.storageService.get('username')
    this.activatedRoute.data.subscribe((data) => {

      //console.log(data);
       this.user = data.user;
        //this.teams = data.teams;
       // this.teams = this.user.teams;



        this.usersDataService.revReqsOfUser(this.user.userId) // own rev reqs
          .subscribe(
            (ownRevReqs) => {
            this.ownRevReqs = ownRevReqs;
        })  

        this.usersDataService.revReqsToReviewByUser(this.user.userId) // own rev reqs
        .subscribe(
          (revReqs) => {
          this.revReqs = revReqs;
      })  
        // this.usersDataService.commentsOfUser(this.user.userId)
        // .subscribe(
        //   (comments) => {
        //     this.comments = comments;
        //   }
        // );

        this.usersDataService.teamsUserIsMemberOf(
           this.user.userId
           //data.user.userId
          //'e49c0e80-bc00-4741-9ba6-0ac6cda01e14'
          )
          .subscribe(
            // on success // data
            (teams) => {
              this.teams = teams; 
              // this.user.teams = teams;
              console.log(teams[0]);

            },
            (error) => {console.log(error)},
            // message 3rd callback
            () => {console.log('3rd callback of teams success')},
        );


    }); // activatedRoute closing '}'
  } // ngOnInit closing '}'

  ngOnDestroy() {
    //this.loggedInUserSubscription.unsubscribe();
  }


}

