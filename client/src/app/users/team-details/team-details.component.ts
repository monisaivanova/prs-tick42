import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserInterface } from '../../common/interfaces/user';
import { UsersDataService } from '../services/users-data.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../core/services/auth.service';
import { NotificatorService } from '../../core/services/notificator.service';
import { RevReqCommentsBaseComponent } from '../../review-requests/rev-req-comments--base.component';
import { RevReqDataService } from '../../review-requests/services/rev-req-data.service';
import { TeamInterface } from '../../common/interfaces/show-team';
import { StorageService } from '../../core/services/storage.service';
import { RevReqInterface } from '../../common/interfaces/show-rev-req';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-team-details',
    templateUrl: './team-details.component.html',
    styleUrls: ['./team-details.component.css']
  })
  export class TeamDetailsComponent implements OnInit, OnDestroy {
  
    public user: any; //UserInterface;
    public loggedInUserSubscription: Subscription;
    public loggedInUser: string;
    public loggedInUserId: string; 
    public teams: TeamInterface[] = []; // TeamDisplayInterface
    public currentJustify = 'justified';
    public isFried: boolean = false;
    public teamRevReqs: RevReqInterface[] = [];
    public revReqsToReviewByTeamMember: RevReqInterface[] = [];

    public team: TeamInterface;

    public addTeamMemberForm: FormGroup;
    public newTeam: TeamInterface;
    
    constructor(
      private readonly notificator: NotificatorService,
      revReqDataService: RevReqDataService,
      private readonly activatedRoute: ActivatedRoute,
      private readonly usersDataService: UsersDataService,
      private readonly authService: AuthService,
      private readonly storageService: StorageService,
      private readonly formBuilder: FormBuilder,

    ) {

    }
  
    ngOnInit(): void {
      this.addTeamMemberForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
      });
      //this.user = this.storageService.get('username')
      this.activatedRoute.data.subscribe((data) => {
  
        console.log(data);
         this.user = data.user;
         this.team = data.team;
         this.team.owner = data.team.owner;
         this.team.reviewRequestsToView = data.team.reviewRequestsToView;
         
         console.log(this.team) 
         //this.teams = data.teams;
         // this.teams = this.user.teams;
  

  
  
          this.usersDataService.revReqsOfTeam(this.team.teamId) // own rev reqs
            .subscribe(
              (teamRevReqs) => {
              this.teamRevReqs = teamRevReqs;
          })  
  
          this.usersDataService.revReqsToReviewByTeamMember(this.team.teamId , this.user.userId) // own rev reqs
          .subscribe(
            (revReqs) => {
            this.revReqsToReviewByTeamMember = revReqs;
        })  
          // this.usersDataService.commentsOfUser(this.user.userId)
          // .subscribe(
          //   (comments) => {
          //     this.comments = comments;
          //   }
          // );
  
          this.usersDataService.teamsUserIsMemberOf(
             this.user.userId
             //data.user.userId
            //'e49c0e80-bc00-4741-9ba6-0ac6cda01e14'
            )
            .subscribe(
              // on success // data
              (teams : TeamInterface[]) => { //>?? change made
                this.teams = teams; 
                // this.user.teams = teams;
                //console.log(teams[0]);
              },
              (error) => {console.log(error)},
              // message 3rd callback
              () => {console.log('3rd callback of teams success')},
          );
  
          // // this.usersDataService.teamsUserIsOwnerOf(this.user.userId)
          // //   .subscribe(
          // //     (teams) => {
          // //       //this.teams = teams;
          // //       this.user.createdTeams = teams;
          // //     }
          // // );
    
  
      }); // activatedRoute closing '}'
    } // ngOnInit closing '}'
  
    public addTeamMember(): void {
      //console.log(this.createRevReqForm.value);
      this.usersDataService.addTeamMember(this.team.teamId,this.addTeamMemberForm.value, this.user).subscribe(
        (teamWithNewMember: TeamInterface) => {
          //console.log(teamWithNewMember)
          this.team.members = teamWithNewMember.members;
          //this.router.navigate([`/${this.teamId}`]);
          // to clear input firld: https://www.danvega.dev/blog/2017/06/07/angular-forms-clear-input-field/
          this.notificator.success('Team member added!');
        },
        (error) => {
          this.notificator.error('Team member not added!');
        }
      );
    }


    ngOnDestroy() {
      //this.loggedInUserSubscription.unsubscribe();
    }
  
  
  }
  
  