
import { of } from 'rxjs';
import { UserInterface } from '../../common/interfaces/user';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NotificatorService } from '../../core/services/notificator.service';
import { UsersDataService } from './users-data.service';
import { catchError } from 'rxjs/operators';
import { TeamInterface } from '../../common/interfaces/show-team';
@Injectable({
  providedIn: 'root'
})
export class TeamDetailsResolverService implements Resolve<TeamInterface[] | {teams: TeamInterface[]}> {
  private teamId: string;

  constructor(
    private readonly usersDataService: UsersDataService,
    private readonly notificator: NotificatorService,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {  
    this.teamId = route.params.teamId;


    return this.usersDataService.singleTeam(this.teamId)
        .pipe(catchError(
          (res) => {
            this.notificator.error(res.error.error);
            console.log("Error at team resolver service.");
            return of({teams: null});
          }
        ));
  }
}
