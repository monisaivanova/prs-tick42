import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserInterface } from '../../common/interfaces/user';
import { WorkItemInterface } from '../../common/interfaces/work-item';
import { CommentInterface } from '../../common/interfaces/show-comment';
import { RevReqInterface } from '../../common/interfaces/show-rev-req';
import { TeamInterface } from '../../common/interfaces/show-team';
import { CreateTeamInterface } from '../../common/interfaces/create-team';

@Injectable({
    providedIn: 'root'
})
export class UsersDataService {
    constructor(
        private readonly http: HttpClient,
    ) { }

    public allUsers(): Observable<UserInterface[]> {
        return this.http.get<UserInterface[]>('http://localhost:3000/api/users');
    }

    public singleUser(userId: string): Observable<UserInterface> {
        return this.http.get<UserInterface>(`http://localhost:3000/api/users/${userId}`);
    }

    // public findUserByUsername(username: string): Observable<UserInterface> { 
    //     return this.http.get<UserInterface>(`http://localhost:3000/api/users/localStorage`);
    // }

    //own rev req-s//where user ===creator
    public revReqsOfUser(userId: string): Observable<RevReqInterface[]> {
        return this.http.get<RevReqInterface[]>(`http://localhost:3000/api/users/${userId}/own-rev-reqs`);
    }
    // todo /// username OR userId...
    public revReqsToReviewByUser(userId: string): Observable<RevReqInterface[]> {
        return this.http.get<RevReqInterface[]>(`http://localhost:3000/api/users/${userId}/rev-reqs`);
    }

    public workItemsOfUser(username: string): Observable<WorkItemInterface[]> {
        return this.http.get<WorkItemInterface[]>(`http://localhost:3000/api/work-items`);
    }

    public findWorkItemByRevReqId(revReqId: string): Observable<WorkItemInterface> {
        return this.http.get<WorkItemInterface>(`http://localhost:3000/api/work-items/${revReqId}/match`);
    }

    // method not yet added 
    // public commentsOfUser(userId: string): Observable<CommentInterface[]> {
    //     return this.http.get<CommentInterface[]>(`http://localhost:3000/api/....`);
    // }

    public teamsUserIsMemberOf(userId: string): Observable<TeamInterface[]> {
        return this.http.get<TeamInterface[]>(`http://localhost:3000/api/teams/${userId}`);
    }

    public teamsUserIsMemberOfByUsername(username: string): Observable<TeamInterface[]> {
        return this.http.get<TeamInterface[]>(`http://localhost:3000/api/teams/${username}/all`);
    }

    // public teamsUserIsOwnerOf(userId: string): Observable<TeamInterface[]> {
    //     return this.http.get<TeamInterface[]>(`http://localhost:3000/api/teams/${userId}/own`);
    // }

    public singleTeam(teamId: string): Observable<TeamInterface[]> {
        return this.http.get<TeamInterface[]>(`http://localhost:3000/api/teams/${teamId}/1`);
    }

    public createTeam(teamName: CreateTeamInterface, user: UserInterface): Observable<CreateTeamInterface> {
        return this.http.post<TeamInterface>(`http://localhost:3000/api/teams`, teamName);
    }

    public addTeamMember(teamId: string, newMember: {email: string}, user: UserInterface ): Observable<object> { // TeamInterface
        return this.http.put<TeamInterface>(`http://localhost:3000/api/teams/${teamId}`, newMember);
    }

    public revReqsOfTeam(teamId: string): Observable<RevReqInterface[]> {
        return this.http.get<RevReqInterface[]>(`http://localhost:3000/api/teams/${teamId}/viewRevReqs`);
    }
    
    public revReqsToReviewByTeamMember(teamId: string, userId:string): Observable<RevReqInterface[]> {
        return this.http.get<RevReqInterface[]>(`http://localhost:3000/api/teams/${teamId}/viewRevReqs/${userId}`);
    }

}

