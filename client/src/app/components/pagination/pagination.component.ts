import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  @Input() public viewing: number;
  @Input() public through: number;
  @Input() public placement: string;
  @Input() public page: number;
  @Input() public pageSize: number;
  @Input() public collectionSize: number;
  @Input() public paginationDisabled: boolean;

  @Output() public newPage = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  public loadPage(page: number): void {
    this.newPage.emit(page);
  }

}
