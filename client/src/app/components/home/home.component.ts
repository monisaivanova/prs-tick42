import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../app/core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  public isLoggedInSubscription: Subscription;
  public isLoggedIn: boolean;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.isLoggedInSubscription = this.authService.user$.subscribe(
      (data) => {
        this.isLoggedIn = !!data;

        if (data) {
          this.router.navigate(['/users/:userId']); //aka DASHBOARD//user-profile // ??
        }
      }
    );

    // document.(function() {
    //   var text = document.(".text");
    //   document.(window).scroll(function() {
    //     var scroll = document.(window).scrollTop();
    
    //     if (scroll >= 200) {
    //       text.removeClass("hidden");
    //     } else {
    //       text.addClass("hidden");
    //     }
    //   });
    // });
  }

  ngOnDestroy() {
    this.isLoggedInSubscription.unsubscribe();
  }

}
