import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../core/services/auth.service';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { Router } from '@angular/router';
import { UserLoginInterface } from './../../common/interfaces/user-login';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersDataService } from '../../users/services/users-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly usersDataService: UsersDataService,
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, 
        // ValidateRegisteredUserEmail  // todo
      ]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  public onLogin(): void {

    const user: UserLoginInterface = { 
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    };

    this.usersDataService.allUsers().subscribe(
      (users: any) => {
        this.notificator.success(`Hello,  ${users.user.username}!`);
     
      },
      () => {
        //this.notificator.error('Something went wrong..'); 
      }
    );
    


    this.authService.login(user).subscribe(
      (data: any) => {
        console.log(data);
        this.notificator.success(`Hello, ${data.user.fullName}!`);
        //this.router.navigate(['/home']);
        //if(data.user.teams)

        this.router.navigate([`/users/${data.user.userId}`]);
      },
      () => {
        this.notificator.error('Login failed!');
      }
    );
  }

}
