import { Subscription } from 'rxjs';
import { AuthService } from './../core/services/auth.service';
import { RevReqDataService } from './services/rev-req-data.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificatorService } from '../core/services/notificator.service';
import { Location } from '@angular/common';
import { SearchService } from '../core/services/search.service';
import { RevReqCommentsBaseComponent } from './rev-req-comments--base.component';
import { RevReqInterface } from '../common/interfaces/show-rev-req';

@Component({
  selector: 'rev-req',
  templateUrl: './rev-req.component.html',
  styleUrls: ['./rev-req.component.css']
})
export class ReviewRequestComponent {//extends RevReqCommentsBaseComponent implements OnInit, OnDestroy {
  // public loggedInUserSubscription: Subscription;
  // public searchSubscription: Subscription;
  // public loggedInUser: string;

  // public paginationPage = 1;
  // public pageSize = 5;
  // public paginationCollectionSize: number;
  // public viewing = 5;
  // public through = 5;
  // public paginationDisabled = false;

  // constructor(
  //   revReqDataService: RevReqDataService,
  //   notificator: NotificatorService,
  //   private readonly activatedRoute: ActivatedRoute,
  //   private readonly authService: AuthService,
  //   private readonly router: Router,
  //   private readonly location: Location,
  //   private readonly searchService: SearchService,
  // ) {
  //   super(revReqDataService, notificator);
  // }

  // ngOnInit() {
  //   this.activatedRoute.data.subscribe(data => this.data = data.revReqs);
  //   this.activatedRoute.data.subscribe(data => this.paginationCollectionSize = data.count);

  //   this.loggedInUserSubscription = this.authService.user$.subscribe(
  //     (user) => this.loggedInUser = user
  //   );

  //   this.activatedRoute.queryParamMap.subscribe(
  //     (params: Params) => {
  //       if (params.params.page) {
  //         this.paginationPage = params.params.page;
  //       }
  //     }
  //   );

  //   this.searchSubscription = this.searchService.search$.subscribe(
  //     (search: string) => {
  //       if (search === '' || search === 'clearTheSearch') {
  //         this.paginationDisabled = false;
  //         this.paginationPage = 1;
  //         this.revReqDataService.allRevReqs().subscribe(
  //           (revReqs: RevReqInterface[]) => {
  //             this.data = revReqs;
  //           }
  //         );
  //       } else if (search !== '' && search !== 'clearTheSearch') {
  //         this.revReqDataService.searchRevReqs(this.paginationPage, search).subscribe(
  //           (revReqs: RevReqInterface[]) => {
  //             this.paginationDisabled = true;
  //             this.data = revReqs;
  //           }
  //         );
  //       }
  //     }
  //   );
  // }

  // ngOnDestroy() {
  //   this.loggedInUserSubscription.unsubscribe();
  //   this.searchSubscription.unsubscribe();

  //   this.paginationDisabled = false;
  //   this.searchService.emitSearch('clearTheSearch');
  // }

  // public onPaginationChange(page: number): void {
  //   this.revReqDataService.allRevReqs(page).subscribe(
  //     (revReqs: RevReqInterface[]) => {
  //       this.data = revReqs;
  //       this.paginationPage = page;

  //       this.through = Math.min((page * this.pageSize), this.paginationCollectionSize);
  //       this.viewing = Math.min(this.pageSize, this.through - ((page * this.pageSize) - (this.pageSize - 1))) + 1;

  //       const url = this.router.createUrlTree([], {relativeTo: this.activatedRoute, queryParams: {page}})
  //         .toString();

  //       this.location.go(url);
  //     },
  //     (error) => this.notificator.error('Something went wrong!')
  //   );
  // }
}
