import { NotificatorService } from '../core/services/notificator.service';
import { RevReqDataService } from './services/rev-req-data.service';
import { UserInterface } from '../common/interfaces/user';
import { CommentInterface } from '../common/interfaces/show-comment';
import { RevReqInterface } from '../common/interfaces/show-rev-req';

export class RevReqCommentsBaseComponent {
  public user: UserInterface;
  public data: RevReqInterface[] = [];
  public comments: CommentInterface[] = []; //todo

  constructor(
      protected readonly revReqDataService: RevReqDataService,//todo
      protected readonly notificator: NotificatorService,
  ) { }

  public approveOnce(workItemId: string, revReqId: string): void { 
      this.revReqDataService.approveOnce(workItemId, revReqId, true).subscribe(
        (revReq: RevReqInterface) => {
          const updatedRevReq = this.data.find((item: RevReqInterface) => item.revReqId === revReq.revReqId);
          const updatedRevReqIndex = this.data.indexOf(updatedRevReq);
          // todo // ?? //not entirely analogical to likes tho ..
          this.data[updatedRevReqIndex].approvedRateRevReqs = revReq.approvedRateRevReqs;

          this.notificator.success('Thanks for your approval, wise man/woman! For the Review Request to be "APPROVED" approved, all other assigned reviewers should follow your steps!');
        },
        (error) => {
          if (error.error.message === 'User has already approved this.') { // todo // tochange
            this.notificator.error('Whoopsi! Only one approval per reviewer! Anyways, thanks, dude, I feel the love...');
          }
        }
      );
    }

  //   public updateItem(item: RevReqInterface): void {
  //   this.revReqDataService.updatePost(item).subscribe(
  //     (post: Post) => {
  //       this.notificator.success('Post updated successfully!');
  //     },
  //     (error) => {
  //       this.notificator.error('Post update failed!');
  //     }
  //   );
  // }

  // public deleteItem(postId: string): void {
  //   this.revReqDataService.deletePost(postId).subscribe(
  //     (post: Post) => {
  //       const deletedPost = this.data.find((item: Post) => item.id === post.id);
  //       const deletedPostIndex = this.data.indexOf(deletedPost);

  //       this.data.splice(deletedPostIndex, 1);
  //       // We need to call the server again

  //       this.notificator.success('Post deteled successfully!');
  //     },
  //     (error) => {
  //       this.notificator.error('Post delete failed!');
  //     }
  //   );
  // }

  // public updateComment(item: CommentInterface): void {
  //   this.revReqDataService.updateComment(item.id, item).subscribe(
  //     (comment: CommentInterface) => {
  //       this.notificator.success('Comment updated successfully!');
  //     },
  //     (error) => {
  //       this.notificator.error('Comment update failed!');
  //     }
  //   );
  // }

  // public deleteComment(commentId: string): void {
  //   this.revReqDataService.deleteComment(commentId).subscribe(
  //     (comment: CommentInterface) => {
  //       const deletedComment = this.comments.find((item: CommentInterface) => item.id === comment.id);
  //       const deletedCommentIndex = this.comments.indexOf(deletedComment);

  //       this.comments.splice(deletedCommentIndex, 1);

  //       this.notificator.success('Comment deteled successfully!');
  //     },
  //     (error) => {
  //       this.notificator.error('Comment delete failed!');
  //     }
  //   );
  // }
}
