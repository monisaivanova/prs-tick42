import { ApplicationRef, ChangeDetectorRef , forwardRef , Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommentInterface } from '../../common/interfaces/show-comment';
import { NotificatorService } from '../../core/services/notificator.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../app/core/services/auth.service';
import { RevReqCommentsBaseComponent } from '../rev-req-comments--base.component';
import { RevReqInterface } from '../../common/interfaces/show-rev-req';
import { RevReqDataService } from '../services/rev-req-data.service';
import { WorkItemInterface } from '../../common/interfaces/work-item';
import { UsersDataService } from '../../users/services/users-data.service';
import { TeamInterface } from '../../common/interfaces/show-team';
import { UserInterface } from '../../common/interfaces/user';
import { StorageService } from '../../core/services/storage.service';
import { CreateComment } from '../../common/interfaces/create-comment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChangeStatusInterface } from '../../common/interfaces/change-status';
import { isApprovedInterface } from '../../common/interfaces/is-approved';
import { DescriptionEditWorkItemInterface } from '../../common/interfaces/description-work-item';
import { TagInterface } from '../../common/interfaces/show-tag';
import { TagNameInterface } from '../../common/interfaces/tag-name';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Button } from 'selenium-webdriver';
import { renderComponent } from '@angular/core/src/render3';
import { TitleWorkItemInterface } from '../../common/interfaces/title-work-item';
import { HttpClient } from '@angular/common/http';


@Component({ 
  selector: 'app-rev-req-details',
  templateUrl: './rev-req-details.component.html',
  styleUrls: ['./rev-req-details.component.css']
})
export class RevReqDetailsComponent extends RevReqCommentsBaseComponent implements OnInit, OnDestroy {
  
  
  public revReq: RevReqInterface;
  public reviewersArray: UserInterface[] = [];
  public reviewersArrayOfUsernames: string[] = [];
  public isReviewer: boolean = false;
  public thisReviewerHasApprovedOnce: boolean = false;
  public workItem: WorkItemInterface; // ?? todo

  public loggedInUserSubscription: Subscription; 
  public loggedInUser: string;
  public username: string;
  public user: UserInterface;
  public isCreator: boolean = false;

  public comment: CommentInterface;
  public comments: CommentInterface[] = [];
  public newComments: CommentInterface[] = [];

  public createCommentForm: FormGroup;  
  public options: Object = {
    placeholderText: 'Write your comment here...',
    //charCounterCount: true,
    heightMin: 100,
    heightMax: 200,
  };

  public editDescriptionForm: FormGroup;
  public editOptions: Object = {
    heightMin: 100,
    heightMax: 200,
  };
  public toggle: boolean = false;
  public toggle2: boolean = false;

  public allTagsArray: any[] = []; //comes from server//TagInterface[] = [];
  public selectedTagsArray: object[] = [];
  public allTagNamesArr: string[] = [];
  public formGroup: FormGroup;
  public viewMe: boolean = false;
  public theUpload: File;
  public url = '';

  constructor(
    private cdr: ChangeDetectorRef,
    private appRef: ApplicationRef,
    revReqDataService: RevReqDataService,
    notificator: NotificatorService,
    private readonly usersDataService: UsersDataService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly storageService: StorageService,
    private readonly formBuilder: FormBuilder,
    private http: HttpClient,

  ) {
    super(revReqDataService, notificator);
  }

  ngOnInit() {
    this.revReqDataService.allTags().subscribe(
      (tagsArr: TagInterface[] ) => {
        this.allTagsArray = tagsArr;
        this.allTagNamesArr = tagsArr.map(tag => tag.name);
      }
    );

    this.createCommentForm = this.formBuilder.group({
      message: ['', [Validators.required, Validators.minLength(3)]],
    }); 
    this.editDescriptionForm = this.formBuilder.group({
      description: ['', [Validators.required, Validators.minLength(3)]],
    }); 
    this.formGroup = this.formBuilder.group({
      file: [null, Validators.required]
    })
          

    this.activatedRoute.data.subscribe(data => {
      console.log("below row is the ngOnInit for rev-req-details.component> ");
      console.log(data); // the rev req returned from resolver

      this.revReq = data.revReq;
      this.revReq.status = data.revReq.status; //Property 'type' does not exist on type 'string'
      this.revReq.tags = data.revReq.tags;
      this.reviewersArray = data.revReq.reviewers;
      this.reviewersArrayOfUsernames = this.reviewersArray.map((r) => r.username)
      for (const ta of this.revReq.tags) {
        
       // this.selectedTagsArray.push(ta);
        // console.log("___________________")
        // console.log(this.selectedTagsArray)
      }

      if (this.reviewersArrayOfUsernames.includes(this.username)) {
        this.isReviewer = true;
      }

      this.comments = this.revReq.comments;


      this.usersDataService.findWorkItemByRevReqId(this.revReq.revReqId)
        .subscribe( 
          (workItemMatch) => {
            this.workItem = workItemMatch;

          }
        )
    });

    this.loggedInUserSubscription = this.authService.user$.subscribe(
      (user) => {
        this.loggedInUser = user; // username string
      }
    );

    this.username = this.storageService.get('username');
    const reviewerFoundArrayOfOne = this.reviewersArray
              .filter( userFound => userFound.username === this.username)
    if (reviewerFoundArrayOfOne.length !== 0) { 
      this.user = reviewerFoundArrayOfOne[0]; 
      this.isReviewer = true;     
    } else if (this.revReq.creator.username === this.username) {
      this.user = this.revReq.creator;  
      this.isCreator = true;   
    }
  }

  ngOnDestroy() {
    this.loggedInUserSubscription.unsubscribe();
  }

  public toggleView() : void {
    this.toggle === false ?  this.toggle = true : this.toggle = false;
  
  }

  public toggleView2() : void {   

    this.toggle2 === false ?  this.toggle2 = true : this.toggle2 = false;
  
  }

  public editWorkItem(): void {
    // const newWorkItem : DescriptionEditWorkItemInterface = {
    //   description: this.createCommentForm.value,
    // }    
    this.toggle = true;
    this.revReqDataService.editWorkItem(this.workItem.workItemId,
      this.editDescriptionForm.value, this.user)
        .subscribe((workItemEdited: WorkItemInterface) => {
        
        this.workItem.description = workItemEdited.description;

        if (this.revReq.status.type === 'Change Requested') { 
          
          const typeFill : ChangeStatusInterface = {
            type: 'Pending' // after change, reset // todo ADD notification to team
          }

          this.revReqDataService.directStatusChange(this.workItem.workItemId,
             this.revReq.revReqId, typeFill).subscribe(
            (newRevReq) => {
              this.revReq.status.type = newRevReq.status.type;
            } // now Change Request is acted upon and it is no longer the status
          );

        }
        
        this.toggleView();

        this.notificator.success('I see changes... in description.. of Work Item... of this Review Request... ');
      },
      (error) => {
        this.notificator.error('Edit of description unsuccessful!');
      }


    );


  }

  public changeRequest(): void {
    if ((this.revReq.status.type === 'Pending')
        || (this.revReq.status.type === 'Under Review')) { // it IS a string
          
      const typeFill : ChangeStatusInterface = {
        type: 'Change Requested'
      }
      this.revReqDataService.directStatusChange(this.workItem.workItemId,
         this.revReq.revReqId, typeFill).subscribe(
        (newRevReq) => {
          this.revReq.status.type = newRevReq.status.type;
        }
      );

      // send NOTIFICATION!
     // const wi = this.....
      const title = this.workItem.title;
      const workItemTitle : TitleWorkItemInterface = {
        title: title,
      }
      this.revReqDataService.notifyForRevReq(this.workItem.workItemId, workItemTitle)
            .subscribe(()=>{ console.log('Check your inbox, you have an e-mail ')});
    }
  }

  public reject(): void {
    if ((this.revReq.status.type === 'Pending')
        || (this.revReq.status.type === 'Under Review')) { // it IS a string
          
      const typeFill : ChangeStatusInterface = {
        type: 'Rejected'
      }
      this.revReqDataService.directStatusChange(this.workItem.workItemId,
          this.revReq.revReqId, typeFill).subscribe(
        (newRevReq) => {
          this.revReq.status.type = newRevReq.status.type;
        }
      );
            // send NOTIFICATION!
     // const wi = this.....
     const title = this.workItem.title;
     const workItemTitle : TitleWorkItemInterface = {
       title: title,
     }
     this.revReqDataService.notifyForRevReq(this.workItem.workItemId, workItemTitle)
           .subscribe(()=>{ console.log('You have mail!')});

    }
  }
  
  public addCommentIfReviewer(): void {

    this.revReqDataService.createCommentOfRevReq(this.workItem.workItemId,
       this.revReq.revReqId, this.createCommentForm.value).subscribe(
      (comment: CommentInterface) => { 
        this.comment = comment;
        this.comment.message = comment.message;
        this.comment.reviewer.email = comment.reviewer.email;
        //this.newComments.push(comment);
        this.comments.push(comment);
        // deffinitely change status !!// customised method for this type only IF current is Pending
        
        console.log(this.revReq.status);
        if (this.revReq.status.type === 'Pending') { // it IS a string
          
          const typeFill : ChangeStatusInterface = {
            type: 'Under Review'
          }
          this.revReqDataService.directStatusChange(this.workItem.workItemId, this.revReq.revReqId, typeFill).subscribe(
            (newRevReq) => {
              this.revReq.status.type = newRevReq.status.type;
            }
          );

        }

        this.notificator.success('Your oppinion matters! Thanks for commenting!');

         // send NOTIFICATION!
        // const wi = this.....
        const title = this.workItem.title;
        const workItemTitle : TitleWorkItemInterface = {
          title: title,
        }
        this.revReqDataService.notifyForRevReq(this.workItem.workItemId, workItemTitle)
              .subscribe(()=>{ console.log('Check your inbox, you have an e-mail :)')});
      
      
      },
      (error) => {
        this.notificator.error('Comment creation unsuccessful!');
      }
    );
  };

  // public createCommentOfRevReq(event: string): void {
  //   const createCommentOfRevReq = {
  //     message: event,
  //   };

  //   this.revReqDataService.createCommentOfRevReq(this.workItem.workItemId, this.revReq.revReqId, ).subscribe(
  //     (comment: CommentInterface) => {
  //       this.comments.push(comment);
  //       this.notificator.success('Your oppinion matters! Comment success!');
  //     },
  //     (error) => {
  //       this.notificator.error('Whoopsii, that comment did not even happen... Use the time to reconsider?');
  //     }
  //   );
  // }


 //itemLike is now addApproval
  public addApproval(): void { 
    if ((this.revReq.status.type === 'Pending')
      || (this.revReq.status.type === 'Under Review')) {

      const approve : isApprovedInterface = {
        isApproved: true,
      }
      console.log(this.thisReviewerHasApprovedOnce)

      if (this.thisReviewerHasApprovedOnce === true) {
        this.notificator.error('You only APPROVE once!');
      }

      const userFound = this.reviewersArray
        .filter( userFound => userFound.username === this.username)
      this.user = userFound[0];

      this.revReqDataService.indirectStatusChange(
          this.workItem.workItemId, this.revReq.revReqId, approve, this.user)
          .subscribe((revReq: RevReqInterface) => {
        this.revReq.approvedRateRevReqs = revReq.approvedRateRevReqs;
        this.revReq.status = revReq.status;
        this.thisReviewerHasApprovedOnce = true;
      
        this.notificator.success('Thanks for Your APPROVAL, you wise man/woman!');
        // send NOTIFICATION!
         // const wi = this.....
         const title = this.workItem.title;
         const workItemTitle : TitleWorkItemInterface = {
           title: title,
         }
         this.revReqDataService.notifyForRevReq(this.workItem.workItemId, workItemTitle)
               .subscribe(()=>{ console.log('Check your inbox, you have an e-mail ')});
      },
      (error) => {
          if (error.error.message === 'User has already liked this.') {
            this.notificator.error('You have already given your Holly APPROVAL.');
          }
      });
    } 
    // else {
    //   this.notificator.error('Approving a Request only possible if its current status is either Pending or Under Review.');

    // }

  }

  public removeReviewer() {
    document.getElementById("eachRevMail").style.color = "green";

  }

  // public updateItem(item: RevReqInterface): void {
  //   this.createCommentOfRevReq.updateRevReq(item).subscribe(
  //     (revReq: RevReqInterface) => {
  //       this.notificator.success('Review Request updated successfully!');
  //     },
  //     (error) => {
  //       this.notificator.error('Review Request update failed!');
  //     }
  //   );
  // }

  // public deleteItem(revReqId: string): void {
  //   this.createCommentOfRevReq.deleteRevReq(revReqId).subscribe(
  //     (revReq: RevReqInterface) => {
  //       this.router.navigate(['/rev-reqs']);
  //       this.notificator.success('Review Request deteled successfully!');
  //     },
  //     (error) => {
  //       this.notificator.error('Review Request delete failed!');
  //     }
  //   );
  // }

  //re-use if time l8r
  // public addTagRevReq(): void {
  //   console.log(this.addTagForm.value);
  //   this.revReqDataService.addTag(this.workItem.workItemId,
  //     this.revReq.revReqId,this.addTagForm.value).subscribe(
      
  //     (tag: any) => { // tagName // could be string,too^^^
  //       //this.revReq.tags.push(tag);
        
  //       this.toggleView2();
  //       this.selectedTagsArray.push(tag);
  //       this.notificator.success('Tag successfully added :)');
  //     },
  //     (error) => {
  //       this.notificator.error('Tag not added ;( Are you sure you are adding an existing tag name, tho?');
  //     }
  //   );
  // }

  // add taf NEW logic: with buttons
  public addTagRevReq(tag): void {
    /// property !!! ^^^^^ major key

    //console.log(tag) 
    // СКРИТА РЕКЛАМА: "VALKATA IS THE BEST"
    // if (tag.name === "important") { 
    //   console.log('important') 
    // } else if (tag.name === "bug report") {
    //   console.log('bug report')
    // } else if (tag.name === "documentation issue"){
    //   console.log('documentation issue')
    // }else if (tag.name === "feature request"){
    //   console.log('feature request')
    // }
    // else if (tag.name === "organisational question"){
    //   console.log('organisational question')
    // }

    const tagNameFill : TagNameInterface = {
      name: tag.name,
    }

    this.revReqDataService.addTag(this.workItem.workItemId,
      this.revReq.revReqId, tagNameFill).subscribe(      
      (revReqUpdated: RevReqInterface) => { // tagName // could be string,too^^^
        this.toggleView2();
        
        //console.log(revReqUpdated.tags.map(tagObj => tagObj.name));
        //const dynamicDisplayTagNamesNew: string = revReqUpdated.tags.map(tagObj => tagObj.name);
        // const stringNameTag = tag;
        // console.log(stringNameTag);

        //console.log(revReqUpdated.tags);
        for (const tagObj of this.revReq.tags) {
          console.log(tagObj)
          const tagobject : any = tagObj;
          if (tagobject.name === tag.name) {
            this.notificator.error('You have already added this tag to the tags list ! We get it.')
            return;
          }      
        } 
        this.revReq.tags = revReqUpdated.tags;
        this.notificator.success('Tag successfully added !');
        
        //this.appRef.tick();
       // this.cdr.detectChanges(); // not loading dynamically- an ng BUG? https://github.com/angular/angular/issues/27875

        //this.tagsNotAddedYetArray.splice(?,1)

                       // send NOTIFICATION!
        // const wi = this.....
        const title = this.workItem.title;
        const workItemTitle : TitleWorkItemInterface = {
          title: title,
        }
        this.revReqDataService.notifyForRevReq(this.workItem.workItemId, workItemTitle)
              .subscribe(()=>{ console.log('Check your inbox, you have an e-mail !')});
           
      },
      (error) => {
        this.notificator.error('Tag not added ;( Are you sure you are adding an existing tag name, tho?');
      },
    );

      // attach file


  }

  public onFileChange(event) {
 
    if(event.target.files && event.target.files.length) {
      const reader = new FileReader();
      this.theUpload = <File>event.target.files[0];
      //const [file] = event.target.files;
      //reader.readAsDataURL(file);
      reader.readAsDataURL(this.theUpload);
      reader.onload = (event) => {


       //this.url = event.target.result;
       this.url = reader.result.toString();

      //  this.formGroup.patchValue({
      //   file: reader.result
      //});

       //this.theUpload = <File>event.target.files[0];
       //event.target = "../../../assets"; // save locally in assets folder
      
       // this.cdr.markForCheck();
      };
    }
  }

  public viewFile() {
    this.viewMe === false ?  this.viewMe = true : this.viewMe = false;
    //const fd = new FormData();
    //fd.append('image', this.theUpload, this.theUpload.name);
    // https://www.youtube.com/watch?v=YkvqLNcJz3Y 
    //https://medium.com/@amcdnl/file-uploads-with-angular-reactive-forms-960fd0b34cb5

    //const path = 'http://localhost:';
    //console.log('check assest folder')
    //return this.http.post(path, fd)
    //let testObject = { 'one': 1, 'two': 2, 'three': 3 };
// Put the object into storage
// localStorage.setItem('testObj', JSON.stringify(fd));
    
// // Retrieve the object from storage
// const retrievedObject = localStorage.getItem('testObj');

// console.log('retrievedObject: ', JSON.parse(retrievedObject));
//this.http.post('', fd).subscribe(res => console.log(res));
    // need to run CD since file load runs outside of zone
    //console.log('interesting: ', this.theUpload);
  }


}
