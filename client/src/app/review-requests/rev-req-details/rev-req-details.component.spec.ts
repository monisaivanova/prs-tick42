import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from '../../app.module';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { AppRoutingModule } from '../../app-routing.module';
import { NotificatorService } from '../../core/services/notificator.service';
import { TimeAgoPipe } from 'time-ago-pipe';
import { LikesDislikesComponent } from '../likes-dislikes/likes-dislikes.component';
import { SharedModule } from '../../shared/shared.module';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from '../../core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { RevReqDetailsComponent } from './rev-req-details.component';
import { ReviewRequestComponent } from '../rev-req.component';
import { ReviewRequestViewComponent } from '../rev-req-view/rev-req-view.component';
import { CreateRevReqComponent } from '../create-rev-req/create-rev-req.component';
import { ReviewRequestRoutingModule } from '../rev-req-routing.module';
import { RevReqDataService } from '../services/rev-req-data.service';

describe('RevReqDetailsComponent', () => {
  let component: RevReqDetailsComponent;
  let fixture: ComponentFixture<RevReqDetailsComponent>;
  // ^^ fixture must be in each 'it' to test anew
  let notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
  // let authenticator = jasmine.createSpyObj('AuthService', ['']);
  // let activatedRoute = jasmine.createSpyObj('ActivatedRoute', []);
  let revReqDataService = jasmine.createSpyObj('RevReqDataService', [
    'singleRevReq',
    'allTags',
    'addTag',
    'createWorkItem',
    'editWorkItem',
    'createRevReq',
    'createCommentOfRevReq',
    'approveOnce',
    'directStatusChange',
    'indirectStatusChange',
    'notifyForRevReq',
  ]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        // added the declarations in the @NgModule metadata in rev-req.module.ts
        ReviewRequestComponent,
        ReviewRequestViewComponent,
        TimeAgoPipe,
        RevReqDetailsComponent,
        LikesDislikesComponent,
        CreateRevReqComponent,
        
      ],
      imports: [
        // added the imports in the @NgModule metadata in rev-req.module.ts
        AppModule,
        FroalaEditorModule,
        FroalaViewModule,
        SharedModule,
        RouterModule,
        ReviewRequestRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        // from app.module
        AppRoutingModule,
        BrowserAnimationsModule,
        CoreModule,
        // solves the error: No provider for ActivatedRoute
        RouterTestingModule.withRoutes(
          [{path: '', component: RevReqDetailsComponent},
         // {path: 'simple', component: SimpleCmp}
        ]
        )
      ],
      providers: [
        // those mocked services in this test file
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: RevReqDataService,
          useValue: revReqDataService,
        },
      ],
    })
    .compileComponents();
  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(RevReqDetailsComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  it('should create the component', () => {
    fixture = TestBed.createComponent(RevReqDetailsComponent);

    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });


});
