import { FormGroup, FormBuilder, Validators , FormArray } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { RevReqDataService } from '../services/rev-req-data.service';
import { RevReqInterface } from '../../common/interfaces/show-rev-req';
import { StorageService } from '../../core/services/storage.service';
import { WorkItemInterface } from '../../common/interfaces/work-item';
import { TeamInterface } from '../../common/interfaces/show-team';
import { TagInterface } from '../../common/interfaces/show-tag';
import { UsersDataService } from '../../users/services/users-data.service';
import { TagNameInterface } from '../../common/interfaces/tag-name';
import { TitleWorkItemInterface } from '../../common/interfaces/title-work-item';
import { CreateRevReq } from 'src/app/common/interfaces/create-rev-req';

@Component({
  selector: 'app-create-rev-req',
  templateUrl: './create-rev-req.component.html',
  styleUrls: ['./create-rev-req.component.css']
})
export class CreateRevReqComponent implements OnInit {
  public createWorkItemForm: FormGroup;
  public workItemId: string; //todo

  public teamsUserIsMemberOf: TeamInterface[] = [];

  public createRevReqForm: FormGroup;
  public revReqId: string;
  public revReq: RevReqInterface;

  public allTagsArray: any[] = []; //comes from server//TagInterface[] = [];
  public selectedTagsArray: string[] = [];
  public allTagNamesArr: string[] = [];
  public options: Object = {
    placeholderText: "Oh, sure, elaborate on your next genius idea! The whole office literally can't... wait 4it...",
    //charCounterCount: true,
    heightMin: 50,
    heightMax: 100,
  };

  //public isSubmitted = false;
  public toggle2: boolean = false;

  public viewCreateWorkItemForm: boolean = true;
  public viewCreateRevReqWindow: boolean = false;
  public viewPickTagsWindow: boolean = false;
  public viewFinalCheckWindow: boolean = false;

  public progressBar25;
  public progressBar50;
  public progressBar75;
  public progressBar100;






  constructor(
    private readonly revReqDataService: RevReqDataService,
    private readonly usersDataService: UsersDataService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
    private readonly activatedRoute: ActivatedRoute,    
    private readonly storageService: StorageService,

  ) { }

  ngOnInit() {
    this.revReqDataService.allTags().subscribe(
      (tagsArr: TagInterface[] ) => {
        this.allTagsArray = tagsArr;
        this.allTagNamesArr = tagsArr.map(tag => tag.name);
        
      }
    );

    const username = this.storageService.get('username');
    this.usersDataService.teamsUserIsMemberOfByUsername(username)
      .subscribe((allTeamsUserIsMemberOf: TeamInterface[])=>{
        this.teamsUserIsMemberOf = allTeamsUserIsMemberOf;
      },
      (error) => {
        this.notificator.error('Accessing teams of user unsuccessful!');
      });
 

    this.createWorkItemForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', [Validators.required, Validators.minLength(3)]],
    });

    this.createRevReqForm = this.formBuilder.group({
      teamName: ['', [Validators.required, Validators.minLength(3)]],
    });


    this.progressBar25 = document.getElementById("twentyfive") as HTMLInputElement;
    this.progressBar50 = document.getElementById("fifty") as HTMLInputElement;
    this.progressBar75 = document.getElementById("seventyfive") as HTMLInputElement;
    this.progressBar100 = document.getElementById("onehundred") as HTMLInputElement;

    
  }

  //before creating a rev req, you need to create a work item:
  // 'api/work-items' >>>  post http request to create : 
  public createWorkItem(): void {
    //console.log(this.createWorkItemForm.value);
    this.revReqDataService.createWorkItem(this.createWorkItemForm.value).subscribe(
      (wi: any) => {
        //console.log(workItem)
        this.workItemId = wi.workItemId;
        const title = wi.title;
        //console.log(this.workItemId)
        // 'api/work-items' >>>  then re-route: -to create its rev=req
        // this.router.navigate([`/${this.workItemId}/review-requests`]);
        this.notificator.success('Work Item successfully created!');
        //document.getElementById('twentyfive').
        // todo // progress bar

        // send NOTIFICATION!
        const workItemTitle : TitleWorkItemInterface = {
          title: title,
        }
        this.revReqDataService.notifyForRevReq(this.workItemId, workItemTitle)
              .subscribe(()=>{ console.log('You have mail!')});

        this.viewCreateWorkItemForm = false;
        this.viewCreateRevReqWindow = true;

        
        this.progressBar25.checked = true;

      },
      (error) => {
        this.notificator.error('Work Item creation unsuccessful!');
      }
    );
  }

  public createRevReq(team): void {
    //console.log(this.createRevReqForm.value);
    const teamFill : CreateRevReq = {
      teamName: team.teamName 
    }
    this.revReqDataService.createRevReq(this.workItemId, 
      // this.createRevReqForm.value
      teamFill
      ).subscribe(
      (revReq: any) => { 
        console.log(team);
        console.log(team.teamName);
        this.revReqId = revReq.revReqId;
        this.revReq = revReq; //!!! for method add tag
        // circular dependency made me comment following row:
        //this.allTagsArray = revReq.tags.map(tagEntity => tagEntity.name);

        // after success, re-route to add reviewers, http put, then : tags 
        //this.router.navigate([`/${this.revReqId}`]);
        this.notificator.success('Review Request successfully created!');
        this.viewCreateRevReqWindow = false;
        this.viewPickTagsWindow = true;

        this.progressBar50.checked = true;

      },
      (error) => {
        this.notificator.error('Review Request creation unsuccessful!');
      }
    );
  };



  // public toggleView2() : void { 
  //   this.toggle2 === false ?  this.toggle2 = true : this.toggle2 = false; 
  // }

  // add taf NEW logic: with buttons
  public addTagRevReq(tag): void {
    /// property !!! ^^^^^ major key

    const tagNameFill : TagNameInterface = {
      name: tag.name,
    }

    this.revReqDataService.addTag(this.workItemId,
      this.revReqId, tagNameFill).subscribe(      
      (revReqUpdated: RevReqInterface) => { // tagName // could be string,too^^^
        //this.toggleView2();

        //console.log(revReqUpdated.tags);
        for (const tagObj of this.revReq.tags) {
          console.log(tagObj)
          const tagobject : any = tagObj;
          if (tagobject.name === tag.name) {
            this.notificator.error('You have already added this tag to the tags list! We get it.')
            return;
          }      
        } 
        this.revReq.tags = revReqUpdated.tags;
        this.notificator.success('Tag successfully added!');

        this.progressBar75.checked = true;


        this.viewPickTagsWindow = false;
        this.viewFinalCheckWindow= true;

      },
      (error) => {
        this.notificator.error('Tag not added ;( Are you sure you are adding an existing tag name, tho?');
      },
    );
  }


  public finalCheck(){
    this.progressBar100.checked = true;


    setTimeout( () => {
      this.router.navigate(['/review-requests/work-items' , this.workItemId , this.revReq.revReqId]);

    }, 600 );
  }


}
