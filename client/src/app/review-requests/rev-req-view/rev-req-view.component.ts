import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RevReqInterface } from '../../common/interfaces/show-rev-req';
import { WorkItemInterface } from '../../common/interfaces/work-item';

@Component({
  selector: 'rev-req-view',
  templateUrl: './rev-req-view.component.html',
  styleUrls: ['./rev-req-view.component.css'],
})
export class ReviewRequestViewComponent implements OnInit {
  @Input() public showContent: boolean;
  @Input() public workItem: WorkItemInterface;
  @Input() public revReq: RevReqInterface;
  @Input() public loggedInUser: string;

  @Output() public liked = new EventEmitter<string>();
  @Output() public updatedItem = new EventEmitter<RevReqInterface>();
  @Output() public deletedItem = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  public itemLike(): void {
    this.liked.emit(this.revReq.revReqId);
  }

  public updateItem(revReq: RevReqInterface): void {
    this.revReq = revReq;
    this.updatedItem.emit(this.revReq);
  }

  public deleteItem(): void {
    this.deletedItem.emit(this.revReq.revReqId);
  }

}
