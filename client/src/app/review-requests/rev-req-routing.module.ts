import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReviewRequestComponent } from './rev-req.component';
import { RevReqsResolverService } from './services/rev-reqs-resolver.service';
import { RevReqDetailsResolverService } from './services/rev-req-details-resolver.service';
import { RevReqCountResolverService } from './services/rev-req-count-resolver.service';
import { RevReqDetailsComponent } from './rev-req-details/rev-req-details.component';
import { CreateRevReqComponent } from './create-rev-req/create-rev-req.component';
import { AuthGuard } from '../auth/auth.guard';

const routes: Routes = [
  { path: '', component: ReviewRequestComponent,
    resolve: {revReqs: RevReqsResolverService,
    count: RevReqCountResolverService} , pathMatch: 'full',  canActivate: [AuthGuard],},

  { path: 'work-items', component: CreateRevReqComponent, canActivate: [AuthGuard], },
  
  //{ path: 'work-items/:workItemId', compononet: ...

  { path: 'work-items/:workItemId/:revReqId', component: RevReqDetailsComponent,
    resolve: {revReq: RevReqDetailsResolverService}, canActivate: [AuthGuard], },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReviewRequestRoutingModule {}
