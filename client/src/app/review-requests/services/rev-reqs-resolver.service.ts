import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute, Params } from '@angular/router';

import { NotificatorService } from '../../../app/core/services/notificator.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { RevReqInterface } from '../../common/interfaces/show-rev-req';
import { RevReqDataService } from './rev-req-data.service';

@Injectable({
  providedIn: 'root'
})
export class RevReqsResolverService {//implements Resolve<RevReqInterface[] | {revReqs: RevReqInterface[]}> {
  public page: number;

  constructor(
    private readonly revReqDataService: RevReqDataService,
    private readonly notificator: NotificatorService,
  ) { }

  // public resolve(
  //   route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot,
  // ) {
  //   if (route.queryParams.page) {
  //     this.page = route.queryParams.page;
  //   } else {
  //     this.page = 1;
  //   }

  //   return this.revReqDataService.allRevReqs(this.page)
  //     .pipe(catchError(
  //       res => {
  //         this.notificator.error(res.error.error);
  //         return of({revReqs: []});
  //       }
  //     ));
  // }
}
