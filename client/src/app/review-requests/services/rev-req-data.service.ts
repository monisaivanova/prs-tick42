import { CreateRevReq } from './../../common/interfaces/create-rev-req'; // ??
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateComment } from '../../common/interfaces/create-comment';
import { CommentInterface } from './../../common/interfaces/show-comment';
import { RevReqInterface } from '../../common/interfaces/show-rev-req';
import { CreateWorkItemInterface } from '../../common/interfaces/create-work-item';
import { WorkItemInterface } from '../../common/interfaces/work-item';
import { TagInterface } from '../../common/interfaces/show-tag';
import { TagNameInterface } from '../../common/interfaces/tag-name';
import { ChangeStatusInterface } from '../../common/interfaces/change-status';
import { isApprovedInterface } from '../../common/interfaces/is-approved';
import { UserInterface } from '../../common/interfaces/user';
import { DescriptionEditWorkItemInterface } from '../../common/interfaces/description-work-item';
import { TitleWorkItemInterface } from '../../common/interfaces/title-work-item';

@Injectable({
  providedIn: 'root'
})
export class RevReqDataService {

  constructor(
    private readonly http: HttpClient,
  ) { } 

  // public allRevReqs(page = 1, user = ''): Observable<RevReqInterface[]> {
  //   return this.http.get<RevReqInterface[]>(`http://localhost:3000/api/      `); //todo
  // }

  // public allRevReqsCount(): Observable<number> {
  //   return this.http.get<number>(`http://localhost:3000/api/     `);//todo
  // }
 
  public searchRevReqs(
    // page = 1, 
    search: string): Observable<RevReqInterface[]> {
    return this.http.get<RevReqInterface[]>(`http://localhost:3000/api/work-items/?search=${search}`);//todo
  }

  public singleRevReq(workItemId: string, revReqId: string): Observable<RevReqInterface> {
    return this.http.get<RevReqInterface>(`http://localhost:3000/api/work-items/${workItemId}/review-requests/${revReqId}`);
  }

  public allTags(): Observable<TagInterface[]> {
    return this.http.get<TagInterface[]>(`http://localhost:3000/api/work-items/tags`);
  }

  public addTag(workItemId: string, revReqId: string, name: TagNameInterface): Observable<RevReqInterface> {
    return this.http.put<RevReqInterface>(`http://localhost:3000/api/work-items/${workItemId}/review-requests/${revReqId}/tags`, name);
  } //RevReqInterface

  //before creating a rev req, you need to create a work item:
  public createWorkItem(workItem: object ): Observable<object> {
    return this.http.post<object>(`http://localhost:3000/api/work-items`, workItem);
  }

  public editWorkItem(workItemId: string,
    newWorkItem: DescriptionEditWorkItemInterface,
    user: UserInterface ): Observable<WorkItemInterface> {
    return this.http.put<WorkItemInterface>(`http://localhost:3000/api/work-items/${workItemId}`, newWorkItem);
  }

  // keep: createRevReq //ready :
  public createRevReq(workItemId: string, revReq: CreateRevReq): Observable<RevReqInterface> {
    return this.http.post<RevReqInterface>(`http://localhost:3000/api/work-items/${workItemId}/review-requests`, revReq);
  }

  // public updateRevReq(revReq: RevReqInterface): Observable<RevReqInterface> {
  //   return this.http.put<RevReqInterface>(`http://localhost:3000/api/           `, revReq); // todo
  // }

  // public deleteRevReq(revReqId: string): Observable<RevReqInterface> {
  //   return this.http.delete<RevReqInterface>(`http://localhost:3000/api/    `); //todo
  // }

  public createCommentOfRevReq( workItemId: string, revReqId: string, message: CreateComment,  ): Observable<CommentInterface> {
    return this.http.post<CommentInterface>(`http://localhost:3000/api/work-items/${workItemId}/review-requests/${revReqId}/comments `, message); 
  }

  // public updateComment(commentid: string, comment: CommentInterface): Observable<CommentInterface> {
  //   return this.http.put<CommentInterface>(`http://localhost:3000/api/      `, comment); //todo
  // }

  // public deleteComment(commentid: string): Observable<CommentInterface> {
  //   return this.http.delete<CommentInterface>(`http://localhost:3000/api/      `); //todo
  // }

  // keep: approveOnce //todo
  public approveOnce(workItemId: string,revReqId: string, approval: boolean): Observable<RevReqInterface>{
    const newApproval = { // as in ApprovedRateRevReqEntity  to be pushed to the RevReq
      isApproved:  approval,
    };

    return this.http.put<RevReqInterface>(`http://localhost:3000/api/work-items/${workItemId}/review-requests/${revReqId}`, approval);
  } 

  // tailored direct status change methods!!!  server: parent method directStatusChange
  public directStatusChange(workItemId: string, revReqId: string, type: ChangeStatusInterface): Observable<RevReqInterface> {
    return this.http.put<RevReqInterface>(`http://localhost:3000/api/work-items/${workItemId}/review-requests/${revReqId}/status`, type);
    
  } 

  public indirectStatusChange(workItemId: string, revReqId: string, isApproved: isApprovedInterface, user: UserInterface): Observable<RevReqInterface> {
    
    return this.http.post<RevReqInterface>(`http://localhost:3000/api/work-items/${workItemId}/review-requests/${revReqId}/status-approve`, isApproved);    
  } // we don't need to add the user in @Req from server controller,

  public notifyForRevReq( workItemId: string, workItemTitle: TitleWorkItemInterface,) {    
    return this.http.post(`http://localhost:3000/api/work-items/${workItemId}/review-requests/notify/rev-req`, workItemTitle);    
  }
  // public notify(workItemId: string,) {    
  //   return this.http.post(`http://localhost:3000/api/work-items/${workItemId}/review-requests/notify`);    
  // }
}
