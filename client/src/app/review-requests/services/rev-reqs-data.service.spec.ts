import { TestBed } from '@angular/core/testing';

import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { RevReqDataService } from './rev-req-data.service';

describe('RevReqDataService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['get', 'r', 'put', 'delete']);

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpClient,
        useValue: http,
      },
    ],
  }));

  it('should be created', () => {
    const service: RevReqDataService = TestBed.get(RevReqDataService);
    expect(service).toBeTruthy();
  });

  it('allRevReqs should return all review requests', () => {
    const service: RevReqDataService = TestBed.get(RevReqDataService);

    http.get.and.returnValue(of([
      {
        revReqId: 'abc',
        creator: 'u',
        userID: '444',
        comments: [],
        createdOn: new Date(),
        touchDateColumn: new Date(),
      },
    ]));

    service.allRevReqs().subscribe(
      (r) => expect(r[0].revReqId).toBe('abc')
    );
  });

  it('allRevReqs should call http.get one time', () => {
    const service: RevReqDataService = TestBed.get(RevReqDataService);

    http.get.calls.reset();

    service.allRevReqs().subscribe(
      (r) => expect(http.get).toHaveBeenCalledTimes(1)
    );
  });

  it('allRevReqsCount should return the amount of all revReqs', () => {
    const service: RevReqDataService = TestBed.get(RevReqDataService);

    http.get.and.returnValue(of(10));

    service.allRevReqsCount().subscribe(
      (c) => expect(c).toBe(10)
    );
  });

  it('allRevReqsCount should call http.get one time', () => {
    const service: RevReqDataService = TestBed.get(RevReqDataService);

    http.get.calls.reset();

    service.allRevReqsCount().subscribe(
      (r) => expect(http.get).toHaveBeenCalledTimes(1)
    );
  });

  it('searchRevReqs should return all found revReqs', () => {
    const service: RevReqDataService = TestBed.get(RevReqDataService);

    http.get.and.returnValue(of([
      {
        revReqId: 'abc',
        creator: 'u',
        userID: '444',
        comments: [],
        createdOn: new Date(),
        touchDateColumn: new Date(),
      },
    ]));

    service.searchRevReqs(1, 'abc').subscribe(
      (r) => expect(r[0].revReqId).toBe('abc')
    );
  });

  it('searchRevReqs should call http.get one time', () => {
    const service: RevReqDataService = TestBed.get(RevReqDataService);

    http.get.calls.reset();

    service.searchRevReqs(1, 'abc').subscribe(
      (r) => expect(http.get).toHaveBeenCalledTimes(1)
    );
  });
   // todo
});
