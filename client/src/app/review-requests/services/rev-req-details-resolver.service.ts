import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { RevReqInterface } from '../../common/interfaces/show-rev-req';
import { RevReqDataService } from './rev-req-data.service';

@Injectable({
  providedIn: 'root'
})
export class RevReqDetailsResolverService implements Resolve<RevReqInterface | {revReq: RevReqInterface}> {

  constructor(
    private readonly revReqDataService: RevReqDataService,
    private readonly notificator: NotificatorService,
  ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const workItemId = route.params['workItemId'];
    const revReqId = route.params['revReqId'];
    console.log('rev-req-details-resolver.service:')
    console.log(this.revReqDataService.singleRevReq(workItemId, revReqId));
    return this.revReqDataService.singleRevReq(workItemId, revReqId)
    
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          return of({revReq: null});
        }
      ));
  }
}
