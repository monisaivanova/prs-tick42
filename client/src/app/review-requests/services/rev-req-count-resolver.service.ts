import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NotificatorService } from '../../../app/core/services/notificator.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { RevReqDataService } from './rev-req-data.service';

@Injectable({
  providedIn: 'root'
})
export class RevReqCountResolverService {//implements Resolve<number | {count: number}> {

  constructor(
    // private readonly revReqDataService: RevReqDataService,
    // private readonly notificator: NotificatorService,
  ) { }

  // public resolve(
  //   route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot,
  // ) {
  //   return this.revReqDataService.allRevReqsCount()
  //     .pipe(catchError(
  //       res => {
  //         this.notificator.error(res.error.error);
  //         return of({count: null});
  //       }
  //     ));
  // }
}
