import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '../shared/shared.module';
import { RevReqDataService } from './services/rev-req-data.service';
import { NotificatorService } from '../core/services/notificator.service';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { TimeAgoPipe } from 'time-ago-pipe';
import { RouterTestingModule } from '@angular/router/testing';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { AuthService } from '../core/services/auth.service';
import { SearchService } from '../core/services/search.service';
import { Location } from '@angular/common';
import {} from 'jasmine';
import { ReviewRequestComponent } from './rev-req.component';

describe('RevReqComponent', () => {
  let component: ReviewRequestComponent;
  let fixture: ComponentFixture<ReviewRequestComponent>;

  const revReqDataService = jasmine.createSpyObj('revReqDataService', [
    'allRevReqs',
    'searchRevReqs',
  ]);
  const notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
  const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['']);
  const authService = jasmine.createSpyObj('AuthService', ['login', 'logout', 'register', 'loggedUser']);
  const searchService = jasmine.createSpyObj('SearchService', ['emitSearch']);
  const location = jasmine.createSpyObj('Location', ['go']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ReviewRequestComponent,
        TimeAgoPipe,
      ],
      imports: [
        SharedModule,
        RouterTestingModule,
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
      ],
      providers: [
        {
          provide: RevReqDataService,
          useValue: revReqDataService,
        },
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRoute,
        },
        {
          provide: AuthService,
          useValue: authService,
        },
        {
          provide: SearchService,
          useValue: searchService,
        },
        {
          provide: Location,
          useValue: location,
        },
      ],
    });
  }));

  beforeEach(() => {
    activatedRoute.data = of([
      {

      },
    ]);
    authService.user$ = of('test');
    activatedRoute.queryParamMap = of({
      params: {
        page: 1,
      },
    });
    searchService.search$ = of('test');

    fixture = TestBed.createComponent(ReviewRequestComponent);
    component = fixture.componentInstance;

    component.loggedInUserSubscription = authService.user$.subscribe();
    component.searchSubscription = searchService.search$.subscribe();

    fixture.detectChanges();
  });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
