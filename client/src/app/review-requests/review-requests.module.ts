import { NgModule } from "@angular/core";
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { TimeAgoPipe } from 'time-ago-pipe';
import { ReviewRequestComponent } from './rev-req.component';
import { ReviewRequestViewComponent } from './rev-req-view/rev-req-view.component';
import { LikesDislikesComponent } from './likes-dislikes/likes-dislikes.component';
import { RevReqDetailsComponent } from './rev-req-details/rev-req-details.component';
import { CreateRevReqComponent } from './create-rev-req/create-rev-req.component';
import { ReviewRequestRoutingModule } from './rev-req-routing.module';
import { TagInputModule } from 'ngx-chips';

@NgModule({
    declarations: [
        ReviewRequestComponent,
        ReviewRequestViewComponent,
        TimeAgoPipe,
        RevReqDetailsComponent, //extends RevReqCommentsBaseComponent
        CreateRevReqComponent,
        LikesDislikesComponent,
    ],
    imports: [
        SharedModule,
        RouterModule,
        ReviewRequestRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        TagInputModule, 
    ],
    exports: [
        ReviewRequestComponent,
        ReviewRequestViewComponent,
    ]
})
export class ReviewRequestsModule { }
