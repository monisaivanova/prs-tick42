import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { of } from 'rxjs';
import { UserLoginInterface } from 'src/app/common/interfaces/user-login';

describe('AuthService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['post', 'delete']);
  const storage = jasmine.createSpyObj('StorageService', ['get', 'set', 'remove']);

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpClient,
        useValue: http,
      },
      {
        provide: StorageService,
        useValue: storage,
      },
    ],
  }));

  it('should be created', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });

  it('login should log the use in', () => {
    const service: AuthService = TestBed.get(AuthService);

    http.post.and.returnValue(of({
      token: 'token',
      user: { username: 'hihi' },
    }));

    const userLogin: UserLoginInterface = {
      email: 'hi@hi.com',
      password: 'password',
    };

    service.login(userLogin).subscribe(
      (res) => {
        expect(res.user.username).toBe('hihi');
      }
    );
  });

  it('login should call http.post', () => {
    const service: AuthService = TestBed.get(AuthService);

    const userLogin: UserLoginInterface = {
      email: 'hi@hi.com',
      password: 'password',
    };

    http.post.calls.reset();

    service.login(userLogin).subscribe(
      () => expect(http.post).toHaveBeenCalledTimes(1)
    );
  });

  it('login should update the subject', () => {
    const service: AuthService = TestBed.get(AuthService);

    http.post.and.returnValue(of({
      token: 'token',
      user: { username: 'hihi' },
    }));

    const userLogin: UserLoginInterface = {
      email: 'hi@hi.com',
      password: 'password',
    };

    service.login(userLogin).subscribe(
      () => {
        service.user$.subscribe(
          (username) => expect(username).toBe('hihi')
        );
      }
    );
  });

  });
