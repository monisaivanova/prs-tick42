import { RevReqInterface } from './show-rev-req';
import { UserInterface } from './user';

export interface WorkItemInterface {
  workItemId: string;
  title: string;
  description: string;
  reviewRequest: RevReqInterface;
  creator: UserInterface;
  createdOn: Date;
}