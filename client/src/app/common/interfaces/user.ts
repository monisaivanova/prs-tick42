import { RevReqInterface } from './show-rev-req';
import { ApprovedRateRevReqInterface } from './approved-rate';
import { CommentInterface } from './show-comment';
import { WorkItemInterface } from './work-item';
import { TeamInterface } from './show-team'

export interface UserInterface {
    userId: string;
    username: string;
    email: string;
    //password: string;
    fullName: string;
    createdOn: Date;
    teams: TeamInterface[];
    createdTeams: TeamInterface[];
    workItems: WorkItemInterface[];
    ownRevReqs:RevReqInterface[];
    reviewRequests: RevReqInterface[];
    comments: CommentInterface[];
    approvedRateRevReqs: ApprovedRateRevReqInterface[];
}
