export interface UserRegisterInterface {
  username: string;
  email: string;
  password: string;
  fullName: string;
}
