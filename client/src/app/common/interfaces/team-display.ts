import { UserInterface } from './user';

export interface TeamDisplayInterface {
    teamId: string;
    teamName: string;
    members: UserInterface[];
}
