import { RevReqInterface } from './show-rev-req';

export interface TagInterface {
    tagId: string;
    name: string;
    reviewRequest: RevReqInterface[] | string;
    isDeleted: boolean;
}
