import { UserInterface } from './user';
import { RevReqInterface } from './show-rev-req';

export interface CommentInterface {
   
    commentId: string; 

    message: string;
   
    reviewRequest: RevReqInterface;
 
    reviewer: UserInterface;
}
