import { RevReqInterface } from './show-rev-req';

export interface StatusInterface {
    statusId: string;
    reviewRequests: RevReqInterface[];
    type: string;
}