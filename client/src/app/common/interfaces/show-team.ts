import { UserInterface } from './user';
import { RevReqInterface } from './show-rev-req';

export interface TeamInterface {
    teamId: string;
    teamName: string;
    owner: UserInterface;
    members: UserInterface[];
    reviewRequestsToView: RevReqInterface[];
}