import { RevReqInterface } from './show-rev-req';
import { UserInterface } from './user';

export interface ApprovedRateRevReqInterface {
  // used to bebe AcceptedRateRevReqInterface
  approvedRateId: string;
  isApproved: boolean;
  reviewRequest: RevReqInterface | string;
  reviewer: UserInterface;
}