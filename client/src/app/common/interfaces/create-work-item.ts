export interface CreateWorkItemInterface {
  title: string;
  description: string;
}
