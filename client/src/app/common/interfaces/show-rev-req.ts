import { UserInterface } from './user';
import { TeamInterface } from './show-team';
import { CommentInterface } from './show-comment';
import { TagInterface } from './show-tag';
import { StatusInterface } from './show-status';
import { ApprovedRateRevReqInterface } from './approved-rate';
import { Status } from './status';

export interface RevReqInterface {
    revReqId: string;
    creator: UserInterface;
    teamOfViewers: TeamInterface; // todo
    reviewers: UserInterface[];
    comments: CommentInterface[]; //todo
    tags: string, // because server is string; //TagInterface[];
    status: Status;//StatusInterface; //| object | string;
    approvedRateRevReqs: ApprovedRateRevReqInterface[];
    createdOn: Date;
}
