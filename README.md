# Peer Review System
### Telerik Academy JS  - Final Project by TICK42


# Make sure to CREATE these 2 files on root level in folder SERVER:
1) .env   file with content:

```
PORT=3000
JWT_SECRET=VerySecr3t!
DB_TYPE=mysql
DB_HOST=localhost
DB_PORT=3306
DB_USERNAME=root
DB_PASSWORD=root
DB_DATABASE_NAME=prs
```
2) ormconfig.json    file with content:

```
{
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "database": "prs",
    "username": "root",
    "password": "root",
    "synchronize": true,
    "logging": false,
    "entities": [
        "src/entities/**/*.ts"
    ],
    "migrations": [
        "src/migration/**/*.ts"
    ],
    "cli": {
        "entitiesDir": "src/entities",
        "migrationsDir": "src/migration"
    }
}

```
# Double check you DID CREATE the 2 files (^^^above^^^):

# Don't forget to smile, also:

### SERVER
```
cd server
npm i
npm run start:dev
npm run seed
```
### CLIENT
```
cd client
npm i
npm run start
```
### BROWSER
```
You can now go to your Google Chrome, ideally, and visit http://localhost:4200/home .
Let the pure joy of reviewing AND being reviewed by your peers, begin !
...
P.S. URLoved 
```



