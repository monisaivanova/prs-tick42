import { Module } from '@nestjs/common';
import { TeamsController } from './teams.controller';
import { TeamsService } from './teams.service';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TeamEntity } from '../entities/teams.entity';
import { UserEntity } from '../entities/users.entity';
import { ConverterService } from '../common/converter.service';

@Module({
  imports: [
    PassportModule.register({defaultStrategy: 'jwt'}), 
    TypeOrmModule.forFeature([TeamEntity, UserEntity]),
  ],
  controllers: [TeamsController],
  providers: [TeamsService, ConverterService],
})
export class TeamsModule {}
