import { IsString, IsEmail } from "class-validator";

export class AddMembersDTO {
  @IsEmail()
  email: string;
}
