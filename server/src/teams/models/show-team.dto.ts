// while for PUT HTTP method we used "class-validator"
// -for createSthDTO, for GET method- we use :
import { Expose, Type } from "class-transformer";
import { UserEntity } from "../../entities/users.entity";
import { ShowMembersDTO } from "./show-members.dto.";
import { ShowUserDTO } from "../../users/models/show-user.dto";
import { ShowRevReqDTO } from "../../review-requests/models/show-rev-req.dto";
import { ReviewRequestEntity } from "../../entities/review-req.entity";

export class ShowTeamDTO {
  @Expose()
  teamId: string;

  @Expose()
  teamName: string;

  @Expose()
  owner: ShowUserDTO; //ShowMembersDTO;//

  @Expose()
  //@Type(() => ShowMembersDTO)
  members: ShowMembersDTO[];

  @Expose()
  reviewRequestsToView: ReviewRequestEntity[]; //ShowRevReqDTO[];
}
