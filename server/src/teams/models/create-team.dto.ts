import { IsString, Length } from 'class-validator';

export class CreateTeamDTO {
  @IsString()
  @Length(3, 15)
  teamName: string;

}
