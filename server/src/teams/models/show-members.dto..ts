import { Expose } from "class-transformer";

export class ShowMembersDTO {
  @Expose()
  userId: string;

  @Expose()
  username: string;

  @Expose()
  fullName: string;
  
}
