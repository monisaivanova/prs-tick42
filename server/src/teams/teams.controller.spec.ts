import { Test, TestingModule } from '@nestjs/testing';
import { TeamsController } from './teams.controller';
import { TeamsService } from './teams.service';

describe('Teams Controller', () => {
  let controller: TeamsController;

  const teamsService = {
    findAll() {},
    findOne() {},
    create() {},
    delete() {},
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TeamsController],
      providers: [{
        provide: TeamsService,
        useValue: teamsService,
      }],
    }).compile();

    controller = module.get<TeamsController>(TeamsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  
});
