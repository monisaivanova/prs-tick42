import { Injectable, BadRequestException } from '@nestjs/common';
import { CreateTeamDTO } from './models/create-team.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../entities/users.entity';
import { Repository } from 'typeorm';
import { TeamEntity } from '../entities/teams.entity';
import { ShowTeamDTO } from './models/show-team.dto';
import { ConverterService } from '../common/converter.service';
import { AddMembersDTO } from './models/add-members.dto';
import { ReviewRequestEntity } from '../entities/review-req.entity';
import { ShowRevReqDTO } from '../review-requests/models/show-rev-req.dto';

@Injectable()
export class TeamsService {
  constructor(
    @InjectRepository(TeamEntity)
    private readonly teamsRepository: Repository<TeamEntity>,

    // @InjectRepository(PostLikeDislike)
    // private readonly votesRepository: Repository<PostLikeDislike>,

    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,

    private readonly converterService: ConverterService,
  ) {}

  async findAllTeams(): Promise<ShowTeamDTO[]> {
    const allTeams = await this.teamsRepository.find();
    return await Promise.all(allTeams.map(async (team: TeamEntity) =>
      this.converterService.convertToShowTeamDTO(team)
    ));
  } 

  async findATeam( teamId: string,): Promise<ShowTeamDTO>{
    const foundTeam = await this.teamsRepository.findOne({
      where: {
        teamId,
      }
    });

    if (!foundTeam) {
      throw new BadRequestException('Been lookin everywhere for a team like this - could not find tho..');
    }

    return this.converterService.convertToShowTeamDTO(foundTeam);

  }  

  async findAllTeamsUserIsMEMBERof(userId: string): Promise<ShowTeamDTO[]> {
    const teamssMember = await this.userRepository.findOne(userId);
    if (!teamssMember) {
      throw new BadRequestException('Why have you not joined any teams ;(... Wait 4it..');
    }

    const allTeamsOfUser = await teamssMember.teams;
    return await Promise.all(allTeamsOfUser.map(async (team: TeamEntity) =>
      this.converterService.convertToShowTeamDTO(team)
    ));
  }

  async findAllTeamsUserIsMEMBERofByUsername(username: string): Promise<ShowTeamDTO[]> {
    console.log(username);
    const teamssMember = await this.userRepository.findOne({
      where:
      {
        username: username,
      }
    });
    if (!teamssMember) {
      throw new BadRequestException('Not joined any teams yet??!?');
    }
 
    const allTeamsOfUser = await teamssMember.teams;
    return await Promise.all(allTeamsOfUser.map(async (team: TeamEntity) =>
      this.converterService.convertToShowTeamDTO(team)
    ));
  }


  async findAllTeamsOWNEDbyUser(userId: string): Promise<ShowTeamDTO[]> {
    const teamssOwner = await this.userRepository.findOne(userId);
    if (!teamssOwner) {
      throw new BadRequestException('Why have you not created any teams yet?? Summon some fellow people as of now!');
    }

    const allTeamsOfUser = await teamssOwner.createdTeams;
    return await Promise.all(allTeamsOfUser.map(async (team: TeamEntity) =>
      this.converterService.convertToShowTeamDTO(team)
    ));
  }


  // one-by-one member adding
  async addMember(teamId: string, newMember: AddMembersDTO,
                    user: UserEntity): Promise<ShowTeamDTO> {
    const teamToUpdate = await this.teamsRepository.findOne({
      where: {
        teamId,
      },
    });
    // *problems occurred while I called addMember method from within createTeam
    if (!teamId) { // *this (no longer) causes problems: somehow interferes with POST method for create team..
      throw new BadRequestException(`Please specify which team you wanna enrich with a new member -aka teamId missing in params ;)`);
    }

    if (!teamToUpdate) { // *this Error used to INTERFERE WITH createTeam
      throw new BadRequestException(`A team with such name does not
      exist yet... Maybe create it?... Maybe search for another team?... Whatever you choose!!!`);
    }

    const teamOwner = await teamToUpdate.owner;
    let currentMembers = await teamToUpdate.members;
    const currentMembersCount = currentMembers.length;

    const memberToAdd = await this.userRepository.findOne({
      // assume you can only add REGISTRATED users
      where: {
        email: newMember.email,
      },
    });

    if (!memberToAdd) { // prolly will interfere with other methods, 2
      throw new BadRequestException(`Hmm.. e-mail ${newMember.email}
        ain\'t registered yet... Send a request to e-mail in quesh? tion?`);
    }

    //should only team OWNERS be able to add new members to the team ?
    // if (teamOwner.userId === user.userId) {
    //   //teamToUpdate.members = members;
    //   console.log('owner recognizes! here are the curent members: '
    //     + await Promise.resolve(teamToUpdate.members)); // empty print
    // }else {
    //   throw new BadRequestException('Try adding members to your OWN teams - cannot touch this team !   .');
    // }

    const alreadyMember = currentMembers.filter(
      (currentMember: UserEntity) => currentMember.email === newMember.email);
    if (alreadyMember.length !== 0) {
      throw new BadRequestException(`User with e-mail ${newMember.email} is already a member of this team.`);
    }
    //(!currentMembers.length) ? (currentMembers[currentMembersCount + 1] = memberToAdd) : (currentMembers[currentMembersCount + 1] = memberToAdd);
    currentMembers[currentMembersCount] = memberToAdd;

  
    const updatedTeam = await this.teamsRepository.save(teamToUpdate);

    return this.converterService.convertToShowTeamDTO(updatedTeam);

  }

  async createTeam(team: CreateTeamDTO, user: UserEntity): Promise<ShowTeamDTO> {
    if (!team.teamName) { 
      throw new BadRequestException(`And your team is called ... ?` );
    }

    const ifTrue = await this.teamsRepository
                  .findOne({where: {teamName : team.teamName}});
    if(ifTrue) {
      throw new BadRequestException(`Name taken. Think of sth different... duh..` );
    } 

    const newTeam: TeamEntity = this.teamsRepository.create(team);

    newTeam.owner = Promise.resolve(user);

    // yet, lettuce try again:
    const ownerAddedAsMemberAutomatically = await this.userRepository.findOne({
      where: {
        userId: user.userId,
      },
    });

    if (!ownerAddedAsMemberAutomatically) {
      throw new BadRequestException(`How did you even get so far... no such user found..`);
    }
    // & since owner is ALWAYS 1st member:
    let currentMembers = await newTeam.members;
    currentMembers[0] = ownerAddedAsMemberAutomatically;

    const savedTeam = await this.teamsRepository.save(newTeam);
    //this.activityService.onPostCreate(savedTeam, user);

    return this.converterService.convertToShowTeamDTO(savedTeam);
    // instead of: return plainToClass(ShowPostDTO, savedPost, { excludeExtraneousValues: true });
  }

  async findRevReqsOfTeamToView( teamId: string,): Promise<ShowRevReqDTO[]>{
    const foundTeam = await this.teamsRepository.findOne({
      where: {
        teamId,
      }
    });

    if (!foundTeam) {
      throw new BadRequestException('Been lookin everywhere for a team like this - could not find tho..');
    }

    const revReqsToView = await foundTeam.reviewRequestsToView;

    return await Promise.all(revReqsToView.map(async (revReq: ReviewRequestEntity) =>
      this.converterService.convertToShowRevReqDTO(revReq)
    ));
  }  
  
  async revReqsToReviewByTeamMember( teamId: string, userId: string): Promise<ShowRevReqDTO[]>{
    const foundTeam = await this.teamsRepository.findOne({
      where: {
        teamId,
      }
    });

    if (!foundTeam) {
      throw new BadRequestException('Been lookin everywhere for a team like this - could not find tho..');
    }

    const revReqsToViewByTeam = await foundTeam.reviewRequestsToView;
    let thisArrHelpYa: any[] = [];
    for (let revRequest of await revReqsToViewByTeam){
      let reviewersAll = await revRequest.reviewers
      for (let reviewer  of reviewersAll) {
        if(reviewer.userId === userId) {
          thisArrHelpYa.push(revRequest);
        }
      }
    } 
    //console.log(thisArrHelpYa);

    // functional programming -approach :  FILTER problem : //todo //??
    // const reviewsAssignedToMember =   Promise.all(  revReqsToViewByTeam // !! Promise.all()
    //   // .map(async (revReq: ReviewRequestEntity) => {
    //   //   (await revReq.reviewers)
    //   .filter(async (revReq: ReviewRequestEntity )=> { // the call -back may be not on-time
    //     //console.log((await revReq.reviewers).filter( async (reviewer: UserEntity) => reviewer.userId === userId ));
    //     const currentReviewers = (await revReq.reviewers);
    //      // .filter( async (reviewer: UserEntity) => reviewer.userId === userId )
    //     const isBoolean = currentReviewers.some( (reviewer: UserEntity) => reviewer.userId === userId );   // use sum method to go through current reviewers and return true if 1 id === userId
    //     console.log(isBoolean  + ' '  +  await revReq.revReqId ); // some returns boolean
    //     return isBoolean;   
    //           // if(reviewer.userId === userId) {
    //           //   return true; // filter, if it returns TRUE, it keeps the element, else, if it returns FALSE, the element is discarded
    //           //   thisArrHelpYa[thisArrHelpYa.length] = revReq;
    //           //   // console.log('==========reviewer.userId=============');
    //           //   // console.log(reviewer.userId);
    //           //   // console.log('===========userId============');
    //           //   // console.log(userId);
    //           //   // console.log('===========revReq============');
    //           //   // console.log(revReq);
    //           //   console.log(thisArrHelpYa); 
    //           //   // ВАЛКА, това ми логва вътре в скоупа напълен арей:
    //           //   // [ ReviewRequestEntity {
    //           //   //   revReqId: 'b003069a-eac7-458d-bb35-5e81f1d07ef3',
    //           //   //   createdOn: 2019-07-20T07:49:28.558Z,
    //           //   //   __promise_creator__: Promise { <pending> },
    //           //   //   __reviewers__: [ [UserEntity], [UserEntity] ],
    //           //   //   __has_reviewers__: true } ]
    //           // } 
    //           // else {
    //           //   return false
    //           // }

    //      //} )
    //     // .forEach(async (reviewer: UserEntity) => { 
    //     //    if(await reviewer.userId === userId) {
    //     //       thisArrHelpYa[thisArrHelpYa.length] = revReq;
    //     //       // console.log('==========reviewer.userId=============');
    //     //       // console.log(reviewer.userId);
    //     //       // console.log('===========userId============');
    //     //       // console.log(userId);
    //     //       // console.log('===========revReq============');
    //     //       // console.log(revReq);
    //     //       await console.log('=============thisArrHelpYa==================');
    //     //       console.log(thisArrHelpYa);

    //     //     }
    //     // })  
        
    //   }));

      //await console.log(await reviewsAssignedToMember)
      
     //console.log('***************reviewsAssignedToMember************************');
     // console.log(reviewsAssignedToMember);
     // reviewsAssignedToMember.then(reviews => console.log(reviews))
      // console.log('**************thisArrHelpYa********************');
      // console.log(thisArrHelpYa);
    // ВАЛКА, но ИЗВЪН локал скоупа логва empty array:  []  


    return await Promise.all(thisArrHelpYa.map(async (revReq: ReviewRequestEntity) =>
      this.converterService.convertToShowRevReqDTO(revReq)
    ));
  }  



}
