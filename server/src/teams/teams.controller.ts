import { Controller,
          Get, 
          Param, 
          Post, 
          Body, 
          Put, 
          Delete, 
          ValidationPipe,
          Req,
          UseGuards,
          Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TeamsService } from './teams.service';
import { CreateTeamDTO } from './models/create-team.dto';
import { ShowTeamDTO } from './models/show-team.dto';
import { AddMembersDTO } from './models/add-members.dto';
import { ShowRevReqDTO } from '../review-requests/models/show-rev-req.dto';

@UseGuards(AuthGuard())
@Controller('api/teams')
export class TeamsController {
  constructor(
    private readonly teamsService: TeamsService,
  ) {}


  @Get()
  async findAllTeams(): Promise<ShowTeamDTO[]>{
    return await this.teamsService
      .findAllTeams();
  }

  @Get(':userId')
  async findAllTeamsUserIsMEMBERof(
    @Param('userId') userId: string,
  ): Promise<ShowTeamDTO[]> {
    return this.teamsService.findAllTeamsUserIsMEMBERof(userId);
  }

  @Get(':teamId/1')
  async findATeam(@Param('teamId') teamId: string,): Promise<ShowTeamDTO>{
    return await this.teamsService
      .findATeam(teamId);
  }

  @Get(':teamId/viewRevReqs')
  async findRevReqsOfTeamToView(@Param('teamId') teamId: string,): Promise<ShowRevReqDTO[]>{
    return await this.teamsService
      .findRevReqsOfTeamToView(teamId);
  }

  @Get(':teamId/viewRevReqs/:userId')
  async revReqsToReviewByTeamMember(
    @Param('teamId') teamId: string,
    @Param('userId') userId: string,): Promise<ShowRevReqDTO[]>{
    return await this.teamsService
      .revReqsToReviewByTeamMember(teamId,userId);
  }

  @Post()
  async createTeam(
    @Body(new ValidationPipe({ whitelist: true,
        transform: true })) team: CreateTeamDTO,
    @Req() request,
    ): Promise<ShowTeamDTO> {
    return await this.teamsService
        .createTeam(team, request.user);
  }


  
  @Get(':userId/own')
  async findAllTeamsOWNEDbyUser(
    @Param('userId') userId: string,
  ): Promise<ShowTeamDTO[]> {
    return this.teamsService.findAllTeamsOWNEDbyUser(userId);
  }

  @Put(':teamId')
  async addMember(
    @Param('teamId') teamId: string,
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
      newMember: AddMembersDTO,
      // email //-the only REQUIRED property of AddMembersDTO
      // out of CreateUserDTO// the other is fullName? -optional
    @Req() request,
    ): Promise<ShowTeamDTO> {
    return await this.teamsService.addMember(teamId, newMember, request.user);
  }

  @Get(':username/all')
  async findAllTeamsUserIsMEMBERofByUsername( @Param('username') username: string,
  ): Promise<ShowTeamDTO[]> {
  return this.teamsService.findAllTeamsUserIsMEMBERofByUsername(username);
  }
}
