const nodemailer = require("nodemailer");
import { Injectable, BadRequestException } from '@nestjs/common';
import { ReviewRequestEntity } from '../entities/review-req.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { WorkItemEntity } from '../entities/work-items.entity';
import { ApprovedRateRevReqEntity } from '../entities/approved-rev-req.entity';
import { StatusEntity } from '../entities/status.entity';
import { TeamEntity } from '../entities/teams.entity';
import { UserEntity } from '../entities/users.entity';
import { TagEntity } from '../entities/tag.entity';
import { ConverterService } from '../common/converter.service';
import { WorkItemsService } from '../work-items/work-items.service';
import { ShowRevReqDTO } from './models/show-rev-req.dto';
import { CreateRevReqDTO } from './models/create-rev-req.dto';
import { Repository } from 'typeorm';
import { AddMembersDTO } from '../teams/models/add-members.dto';
import { AddTagDTO } from './tags/models/add-tag.dto';
import { AddStatusDTO } from './status/models/add-status.dto';
import * as util from 'util' 
import { stringify, StringifyOptions } from 'querystring';
import { ShowTagNameDTO } from './tags/models/show-tag-name.dto';
import { ShowCommentDTO } from '../review-requests/models/show-comment.dto';
import { CreateCommentDTO } from '../review-requests/models/create-comment.dto';
import { CommentEntity } from '../entities/comments.entity';
import { AddApprovedRateRevReqDTO } from './approved-rate/models/add-approved-rate.dto';
import { TitleWorkItemDTO } from '../work-items/models/title.dto';


@Injectable()
export class ReviewRequestsService {
    constructor(
        @InjectRepository(ReviewRequestEntity)
        private readonly revReqRepository: Repository<ReviewRequestEntity>,
        @InjectRepository(WorkItemEntity)
        private readonly workItemRepository: Repository<WorkItemEntity>,
        @InjectRepository(ApprovedRateRevReqEntity)
        private readonly approvedRateRevReqRepository: Repository<ApprovedRateRevReqEntity>,
        @InjectRepository(StatusEntity)
        private readonly statusRepository: Repository<StatusEntity>,
        @InjectRepository(TeamEntity)
        private readonly teamRepository: Repository<TeamEntity>,
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        @InjectRepository(TagEntity)
        private readonly tagRepository: Repository<TagEntity>,
        @InjectRepository(CommentEntity)
        private readonly commentsRepository: Repository<CommentEntity>,
        
        private readonly converterService: ConverterService,
    
    ){}

    async findOneRevReq(workItemId: string, revReqId: string): Promise<ShowRevReqDTO> {
        const foundRevReq = await this.revReqRepository.findOne({
          where: {
            revReqId,
          }
        });
    
        if(!foundRevReq) {
          throw new BadRequestException('Whoops! No such Review Request out there..');
        }

        const foundWorkItem = await this.workItemRepository.findOne({
            where: {
               workItemId,
              reviewRequest: foundRevReq,
            }
          });
      
          if(!foundWorkItem) {
            throw new BadRequestException('Whoops! No such Work Item out there to give content to the Review Request...');
          }
    
        return await this.converterService.convertToShowRevReqDTO(foundRevReq);
      }


    // Create review request
    // Link to workItem include in controler parameter id
    // POST method without a body ;)
    async createRevReq(workItemId: string,
         teamName: CreateRevReqDTO, 
        rRcreator: UserEntity,): Promise<ShowRevReqDTO>{
        // prep for newRevReq 1/5 : define "team" property
        const teamOfViewers = await this.teamRepository.findOne({
             where: {
                 teamName : teamName.teamName,
             }
        })
        if(!teamOfViewers) {
             throw new BadRequestException('Whoops! No such team name out there..');
        }
        //const team = Promise.resolve(teamOfViewers); // ??
      
        //link to workItem prep:
        const foundWorkItem = await this.workItemRepository.findOne({
            where: {
                workItemId,
            }
        })


        // must be only 1 revReq per workItem:
        const revReqCheck = await foundWorkItem.reviewRequest;
        if(revReqCheck !== undefined) {
            throw new BadRequestException('Whoops! Looks like there already is a review request for that work item - only one allowed, sorryyy.. not sorry ;( ');
        }

        // check if Rev Req creator =member of that team:
        const teamCheck = await teamOfViewers.members;
        const memberCheck = await foundWorkItem.creator;
        //MUST BE A MEMBER OF THAT TEAM!!
        if(!teamCheck.some(member => member.email === memberCheck.email)) {  
            throw new BadRequestException('Cannot assign viewers of a team you are not a member of, tho...');
        }

        // prep for newRevReq 2/5 : define "creator" property
        if (rRcreator.email !== (await foundWorkItem.creator).email) {
            throw new BadRequestException('Review Request creator must be the same as the work Item creator! Not you? Knew it..');
        } 
        const creatorRr = await this.userRepository.findOne({where: {email: rRcreator.email}})

        // prep for newRevReq 3/5 : define "status" property as type Pending    
        const statusPending = await this.statusRepository.findOne({where: {type: 'Pending'}});
        //console.log(statusPending); // finds it 
        // prep for newRevReq ?/5 : define "reviewers" ?? Y/N? L8R ?
        // prep for newRevReq 0/5 : define "comments" - L8R
        // prep for newRevReq ?/5 : define "tags" ?? Y/N? L8R ?   BODY??
        // prep for newRevReq ?/5 : define "approvedRateRevReqs" ?? Y/N? L8R ? 

        // prep for newRevReq 4/5 :  create an object to '.create'
        
        const revReqObj = {
            //creator: Promise.resolve(creatorRr), //could be not a UserEntity !!
            //teamOfViewers: null, //Promise.resolve(teamOfViewers), //ok
            //reviewers: teamOfViewers.members, //ok// ?? add automatically whole team ?? & then remove?
            //comments: null, // @ 1st
            //tags: null, // ?? or set in body upon creation ??
            //status: Promise.resolve(statusPending),
            //approvedRateRevReqs: null, // @ 1st
            createdOn: new Date(),            
        }  
  
        // prep for newRevReq 5/5 : '.create' // OOR: OMIT AND JUST '.save'
        const newRevReq: ReviewRequestEntity = this.revReqRepository.create(revReqObj);             
        const savedRevReq = await this.revReqRepository.save(newRevReq);
        //!!! https://stackoverflow.com/questions/16594672/1452-cannot-add-or-update-a-child-row-a-foreign-key-constraint-fails
        // saved revReq property fill 1/4 : add teamOfViewers
        teamOfViewers.reviewRequestsToView[(await teamOfViewers.reviewRequestsToView).length] = savedRevReq;
        await this.teamRepository.save(teamOfViewers);
        savedRevReq.teamOfViewers = Promise.resolve(teamOfViewers);
        // saved revReq property fill 2/4 : add creator // !! ownRevReqs - one user-this user
        creatorRr.ownRevReqs[(await creatorRr.reviewRequests).length] = savedRevReq;
        await this.userRepository.save(creatorRr);
        savedRevReq.creator = Promise.resolve(creatorRr);
        // saved revReq property fill 3/4 : add status Pending 
        //(await statusPending.reviewRequests).push(savedRevReq); // CIRCULAR REFERENCE ERROR MADE ME comment THIS
        await this.statusRepository.save(statusPending);
        savedRevReq.status = Promise.resolve(statusPending);
        // console.log(await statusPending.reviewRequests);
        // console.log((await statusPending.reviewRequests).length);
        // console.log(await savedRevReq.status);
        // saved revReq property fill 4/4 : add default reviewers=all members of team
                //(await teamOfViewers.members).forEach(async user => (await user.reviewRequests).push(savedRevReq),0);
                //(await teamOfViewers.members).forEach(async user => await this.userRepository.save(user),0);   
        //add all tags pre0defined
        if(!foundWorkItem) {
            throw new BadRequestException('Whoops! No such Work Item out there..');
        }
        // const tagsAll = await this.tagRepository.find(({
        //     where: {
        //       isDeleted: false,
        //     },
        //   }) );
        // //console.log(tagsAll); //ok 
        // if(!tagsAll) {
        //         throw new BadRequestException('Whoops! No tags..');
        // }
        // WAIT!! you would NOT want all tags !!! automatically!!
        //make a method get all tags
        // tagsAll.map( async tag => {
        //     ((await tag.reviewRequest).push(savedRevReq))
        // });
        //tagsAll.map( async tag => (await this.tagRepository).save(tag)); 
        //await this.tagRepository.save(tagsAll);
        //console.log(tagsAll);               
        //savedRevReq.tags = Promise.resolve(await tagsAll);  // bug
        // throws circular Reference Error

        const reviewersWithoutCreator = Promise.resolve((await teamOfViewers.members)
            .filter((member) =>  member.userId !== creatorRr.userId));
        savedRevReq.reviewers = reviewersWithoutCreator;
        /////link to workItem : One-to-One
        foundWorkItem.reviewRequest = Promise.resolve(savedRevReq); 
        await this.workItemRepository.save(foundWorkItem);
        // major key to success ^^^^

        
        await this.revReqRepository.save(savedRevReq);
        return await this.converterService.convertToShowRevReqDTO(savedRevReq);
        // todo work on  the converter service ^^^ method and DTO itself - statuses
    }


    // Assign reviewers one time action // only from the team
    // Put method to the newly created review request
    async addReviewer(revReqId: string, reviewerMail: AddMembersDTO): Promise<ShowRevReqDTO>{
        const foundRevReq = await this.revReqRepository.findOne({
            where: {
                revReqId, 
            }
        });

        //const foundReviewer = 
        const teamCheck = await (await foundRevReq.teamOfViewers).members;
        if(!teamCheck.some(member => member.email === reviewerMail.email)) {  
            throw new BadRequestException('Cannot assign a reviewer who is not a member of the project team. You can add him/her to the team and try again, tho ;)');
        }

        const foundReviewer = await teamCheck.filter(member => member.email === reviewerMail.email);
        let reviewersArr = await foundRevReq.reviewers;
        const currentReviewersCount = reviewersArr.length;
        reviewersArr[currentReviewersCount] = foundReviewer[0];

        // todo : add condition to only add a reviewer once

        const savedRevReq = await this.revReqRepository.save(foundRevReq);
        //await this.revReqRepository.save(foundRevReq);
        return await this.converterService.convertToShowRevReqDTO(savedRevReq);
    }



    // Assign tag to tag list
    // Put method to the same review request
    // Includes notification in frontend
    // todo: add in separate tag service??
    async addTag(revReqId: string, tagName: AddTagDTO, ): Promise<ShowRevReqDTO>{
        const foundRevReq = await this.revReqRepository.findOne({
            where: {
                revReqId,
            }
        });
        const tagToAdd = await this.tagRepository.findOne({
            where: {
                name: tagName.name, //eg: important
            }
        });

        if(!tagToAdd) {  
            throw new BadRequestException('Datt Tag natt in da dataBase ;) aka know your Tags fore you enroll m! !! How you dzuen tho? Cool blouse you wearin...');
        }        

        let tagsArr = await foundRevReq.tags;
        
        //(await tagToAdd.reviewRequest).push(foundRevReq); // commented due to Circular dependency MAJOR bug I got , choices and priorities... I understand now I cannot search all certain-way-tagged rev reqs, but at least I got out of the circle
        await this.tagRepository.save(tagToAdd);
        (await tagsArr).push(tagToAdd); // NEW!
        //(await foundRevReq.tags).push(tagToAdd);
        const savedRevReq = await this.revReqRepository.save(foundRevReq);

        // todo // NOTIFY team... for the change in tags, unless: 1st tags..
        // todo // think of a way to 1st add tags upon creation of revReq
        return await this.converterService.convertToShowRevReqDTO(savedRevReq);
    }


    // remove tag
    // to remove circular dependency, removed method main logic
    async removeTag(revReqId: string, tagName: AddTagDTO, ): Promise<ShowRevReqDTO>{
        const foundRevReq: ReviewRequestEntity | string = await this.revReqRepository.findOne({
            where: {
                revReqId,
            }
        });
        const tagToRemove = await this.tagRepository.findOne({
            where: {
                name: tagName.name, //eg: important
            }
        });

        if(!tagToRemove) {  
            throw new BadRequestException('No such tag ;*');
        }        

        let tagsArr = await foundRevReq.tags;
         // to remove circular dependency, removed method main logic
       // const foundRevReqIndex = (await tagToRemove.reviewRequest).indexOf(foundRevReq);
       // (await tagToRemove.reviewRequest).splice(foundRevReqIndex, 1); // delete 1 element at the index of the found revReq
        await this.tagRepository.save(tagToRemove);

        const savedRevReq = await this.revReqRepository.save(foundRevReq);

        // todo // NOTIFY team... for the change in tags, unless: 1st tags..
        // todo // think of a way to 1st add tags upon creation of revReq
        return await this.converterService.convertToShowRevReqDTO(savedRevReq);
    }


    // add comment to a review request: be a REVIEWER to 2datt
    async addComment(workItemId: string,  // workItemId needed 4controller
                    revReqId: string,
                    message: CreateCommentDTO,
                    user: UserEntity): Promise<ShowCommentDTO>{
        const foundRevReq = await this.revReqRepository.findOne({
            where: {
                revReqId, 
            }
        });

        const teamCheck = await (await foundRevReq.teamOfViewers).members;
        if(!teamCheck.some(member => member.email === user.email)) {  
            throw new BadRequestException('Cannot comment if not assigned as a reviewer!');
        }

        const newComment = await this.commentsRepository.create(message);
        // ^^ above just saved newComment.message to teh Body of the POST req.
        newComment.reviewer = Promise.resolve(user);
        newComment.reviewRequest = Promise.resolve(foundRevReq); 
        const savedComment = await this.commentsRepository.save(newComment); // ??

        // ?? not? yes?
        //let commentsArr = await foundRevReq.comments;
        //commentsArr.push(newComment);
        // must update revReq also.. automatically?

        const savedCommentEntity = await this.commentsRepository.save(newComment);
        //await this.revReqRepository.save(foundRevReq);
        return await this.converterService.convertToShowCommentDTO(savedCommentEntity);
    }


    // Direct Change Status / Not valid for APPROVAL status only for statuses that can be directly change by each individual reviewer
    // Under Review, Change Requested, Rejected
    // Under Review: only first comment or first accept
    // Change Requested: on each change requested
    // Rejected: on rejected (on first and last reject on a reviewer)
    // If else needed for status to change to Under Review for example any reviewer accepting, 
    async directStatusChange(workItemId: string,revReqId: string, statusType: AddStatusDTO,): Promise<ShowRevReqDTO>{
        const foundRevReq = await this.revReqRepository.findOne({
            where: {
                revReqId,
            }
        });
        const currentStatus = await foundRevReq.status;

        const statusToAssignDirectly = this.statusRepository.findOne({
            where: {
                type: statusType.type,
            }
        });

        if(!statusToAssignDirectly) {  
            throw new BadRequestException('Dass Status nos ins das dataBase ;) aka know your Stats fore you enroll m! Also, you are a Gift To the Universe and deserve all the best of joy and laughter out there!');
        }

        const foundStatusToAssignDirectly = await statusToAssignDirectly;
        // if(foundStatusToAssignDirectly.type === 'Approved' || foundStatusToAssignDirectly.type === 'Pending') {  
        //     throw new BadRequestException('The chosen status is NOT a DIRECT status change. Invalid request. Hope you still see the bright side of that hellava life you livin ...');
        // }


        if(foundStatusToAssignDirectly.type === 'Rejected') {
            foundRevReq.status = Promise.resolve(foundStatusToAssignDirectly);
            // lock the work item ,below if-s do that ;)
            // + glowing ribbon: https://codepen.io/manab_27/pen/KqzbB with rejected
            // same ,yet opposite will be for the indirect Approved status change
            console.log('status was a bit rejjy');
        } else if(foundStatusToAssignDirectly.type === 'Change Requested' 
                             && currentStatus.type !== 'Rejected' // once rejjy, always rejjy
                             && currentStatus.type !== 'Approved' // same goes here, just opposite
                             ) {
            foundRevReq.status = Promise.resolve(foundStatusToAssignDirectly);
            // lock the CHANGE STATUS OPTION UNTIL CHANGE MADE in the work item !!! then allow
            // + glowing ribbon: https://codepen.io/manab_27/pen/KqzbB with rejected
            console.log('status to change plss');

        } else if(foundStatusToAssignDirectly.type === 'Under Review'
                             && currentStatus.type !== 'Rejected' // once rejjy, always rejjy
                             && currentStatus.type !== 'Approved' // same goes here, just opposite
                             ) {
            // this option will be used by the app, aka
            // user interaction will change status to this one Under Review
            foundRevReq.status = Promise.resolve(foundStatusToAssignDirectly);
            //saving done below.... may need to get up here tho
            console.log('Ok, Business Logic, status changed to Under Review');

        } else if(foundStatusToAssignDirectly.type === 'Pending'
                             && currentStatus.type !== 'Rejected' // once rejjy, always rejjy
                             && currentStatus.type !== 'Approved' // same goes here, just opposite
                             ) {
            // this option will be used by the app, aka default +
            // user interaction AFTER making a change to a Requested Change by a reviewer
            foundRevReq.status = Promise.resolve(foundStatusToAssignDirectly);
            console.log('You can\'t directly change to Pending');//err throw?
            //change status to Under Review, for any action on rev req, 
            //e.g.: 'status change', tag add, comment
        } //else // aka sth crazy and not one of the possible statuses // throw err

        const savedRevReq = await this.revReqRepository.save(foundRevReq);
        // todo // NOTIFY team... for the change in tags, unless: 1st tags..
        // todo // think of a way to 1st add tags upon creation of revReq
        return await this.converterService.convertToShowRevReqDTO(savedRevReq);
    }



    // Indirect change status - approve only valid for accepted/approved status
    // Will update +1 approval by individual reviewer
    // When in the linked ApprovedRateRevReqEntity to the same review request the number of approvals
    // by reviwers has reached the number of all reviwers then indirectly status will be changed to approved
    // After APPROVED in frontend mark as complete and how to make if else statement readonly
    async approveAsIndirectStatusChange(
        workItemId: string,
        revReqId: string,
        isApproved: AddApprovedRateRevReqDTO,
        user: UserEntity): Promise<ShowRevReqDTO>{

        console.log('method called by client')

        const foundRevReq = await this.revReqRepository.findOne({
            where: {
                revReqId,
            }
        });
        
        const currentStatus = await foundRevReq.status;
        // console.log('******************************')
        // console.log(currentStatus)
        const currentApprovedRateArrayOfRates = await foundRevReq.approvedRateRevReqs;
        // console.log('***********currentApprovedRateArrayOfRates*******************')
        // console.log(currentApprovedRateArrayOfRates)
        const reviewers = await foundRevReq.reviewers;
        // console.log('************reviewers******************')
        // console.log(reviewers)
        const reviewersCount =  (await foundRevReq.reviewers).length;
        //console.log('*************reviewersCount*****************')
        //console.log(reviewersCount)
        let approvalsCount = (await foundRevReq.approvedRateRevReqs).length;
        //console.log('************approvalsCount******************')
        //console.log(approvalsCount)
        //add condition : only reviewers can approve:
        //console.log('*************!reviewers.includes(user)*****************')
        //console.log(!reviewers.includes(await user))
        const userIsAReviewer = reviewers.some(rev => rev.username === user.username)
        if(!userIsAReviewer) {  
            console.log(`so reviewers do not include user ${await (await user.username )}?`)

            throw new BadRequestException('Sorry, you are not in the reviewers list, so your approval, though hiiighly appreciated, is not needed ;*');
        }
        console.log('passes to this row?')

        const statusUnderReviwIfCurrentIsPending = this.statusRepository.findOne({
            where: {
                type: 'Under Review',
            }
        });

        if(!statusUnderReviwIfCurrentIsPending) {  
            throw new BadRequestException('Dass Status nos ins das dataBase ;) aka know your Stats fore you enroll m! Also, you are a Gift To the Universe and deserve all the best of joy and laughter out there!');
        }
        const foundStatusUnderReviw = await statusUnderReviwIfCurrentIsPending;
        

        const statusToAssignInDirectlyIfConditionMatched = this.statusRepository.findOne({
            where: {
                type: 'Approved',
            }
        });

        if(!statusToAssignInDirectlyIfConditionMatched) {  
            throw new BadRequestException('Dass Status nos ins das dataBase ;) aka know your Stats fore you enroll m! Also, you are a Gift To the Universe and deserve all the best of joy and laughter out there!');
        }
        const foundStatusToAssignInDirectly = await statusToAssignInDirectlyIfConditionMatched;
        
        //add condition : a single reviewer can't approve twice!
        //if this whole method is triggered => +1 approval to add to their count
        //but that wil,too, happen indirectly,with POSTing new approval Entities:
        const preventDoubleApprovalSameReviewer = this.approvedRateRevReqRepository.findOne({
            where: {
                type: 'Approved',
                reviewer: user,
                reviewRequest: foundRevReq.revReqId,
            }
        });
        console.log(await preventDoubleApprovalSameReviewer)

        if((await preventDoubleApprovalSameReviewer) !== undefined) {  
            throw new BadRequestException('You have already approved this Request! If all other reviewers approve the request, its status will be changed to Approved.');
        }
        
        const newApproval: ApprovedRateRevReqEntity = this.approvedRateRevReqRepository.create(isApproved);     
        
        newApproval.reviewRequest = await foundRevReq.revReqId;    // ?? 
        newApproval.isApproved = true;
        newApproval.reviewer = Promise.resolve(user);


        await this.approvedRateRevReqRepository.save(newApproval);      
        (await foundRevReq.approvedRateRevReqs).push(newApproval);
        approvalsCount +=1;
        //console.log('count approvals match reviewers?')
        //console.log(reviewersCount ===approvalsCount)
        if (reviewersCount === approvalsCount) {
            //only then change status of rev req to Approved
            foundRevReq.status = Promise.resolve(foundStatusToAssignInDirectly);
        } else if ((reviewersCount === approvalsCount) 
                    && ((await foundRevReq.status).type === 'Pending')) {
            //only then change status of rev req to Approved
            foundRevReq.status = Promise.resolve(foundStatusToAssignInDirectly);
        } else if ((reviewersCount !== approvalsCount) 
                && ((await foundRevReq.status).type === 'Pending')){
                //only then change status of rev req to Approved
            foundRevReq.status = Promise.resolve(foundStatusUnderReviw);
        } 


        const savedRevReq = await this.revReqRepository.save(foundRevReq);
        // todo // NOTIFY team... for the change in tags, unless: 1st tags..
        // todo // think of a way to 1st add tags upon creation of revReq
        console.log(await this.converterService.convertToShowRevReqDTO(savedRevReq));
        return await this.converterService.convertToShowRevReqDTO(savedRevReq);
    }


    // Put method to the same review request entity
    // Frontend issue to act diffrently on different status
    // This statuses will be checked for if else statements in the frontend logic
    // STATUS reset to Pending!!!

    async notify() {
        nodemailer.createTestAccount((err, account) => {

            const transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 587,
                secure: false,
                auth: {
                    user: 'prspeerrev@gmail.com',
                    pass: 'p1a2s3s4',
                },
                tls: {
                    rejectUnauthorized: false,
                },
            });

            const mailOptions = {
                from: 'prspeerrev@gmail.com',
                to: 'prspeerrev@gmail.com',
                subject: 'PRS',
                text: 'Something is going on in PRS...',
                html: `  Dear Dazzling Creature,<br/> <br/>
                There have been updates in PRS! </b>!
                <hr>
               <i>
                PRS - "The System you DO wanna be a part of!"</i>
                `,
            };

            transporter.sendMail(mailOptions, (error) => {
                console.log('mail sent?')
                if (error) {
                    // error
                    console.log(error)
                }
            });
        });
    }

    async notifyForRevReq(workItemTitle: TitleWorkItemDTO,) {
        nodemailer.createTestAccount(async (err, account) => {
            const workItem = await this.workItemRepository.findOne({
                where: {
                    title: workItemTitle.title,
                }
            })
            const workItemId =  workItem.workItemId;
            const revReq = await (await workItem).reviewRequest;
            const revReqId = "efb22e20-2cb0-41a0-9aba-9d00302c810c";//await (await revReq).revReqId;


            const transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 587,
                secure: false,
                auth: {
                    user: 'prspeerrev@gmail.com',
                    pass: 'p1a2s3s4',
                },
                tls: {
                    rejectUnauthorized: false,
                },
            });

            const mailOptions = {
                from: 'prspeerrev@gmail.com',
                to: 'prspeerrev@gmail.com',
                subject: 'Psst, it\'s PRS',
                text: 'Something is going on in PRS...',
                html: `  Dear Dazzling Creature,<br/> <br/>
                There have been updates in Review Request <b>"${workItemTitle.title}" </b>!
                Click <a href="http://localhost:4200/review-requests/work-items/${workItemId}/${revReqId}"  target="_blank">here</a> to find out more.
                <hr>
               <i>
                PRS - "The System you DO wanna be a part of!"</i>
                `,
            };

            transporter.sendMail(mailOptions, (error) => {
                console.log('mail sent?')
                if (error) {
                    // error
                    console.log(error)
                }
            });
        });
    }



}
