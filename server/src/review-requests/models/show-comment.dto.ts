import { Expose } from "class-transformer";
import { ShowRevReqDTO } from "../../review-requests/models/show-rev-req.dto";
import { ShowUserDTO } from "../../users/models/show-user.dto";
import { ReviewRequestEntity } from "../../entities/review-req.entity";

export class ShowCommentDTO {
    //todo
    @Expose()
    commentId: string; 

    @Expose()
    message: string;

    @Expose()
    reviewRequest: ReviewRequestEntity | string; //ShowRevReqDTO 

    @Expose()
    reviewer: ShowUserDTO;
}
