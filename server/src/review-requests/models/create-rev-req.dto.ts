// no body for that POST req ;)
import { IsString, Length } from "class-validator";
import { ShowTeamDTO } from "../../teams/models/show-team.dto";

export class CreateRevReqDTO { //= createTeamDTO
    @IsString()
    @Length(3, 15)
    //teamOfViewers: ShowTeamDTO;
    // or maybe link to a team from here?
    teamName: string;
    // this will be used as a link to a team

}
