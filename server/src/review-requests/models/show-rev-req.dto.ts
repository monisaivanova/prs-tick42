import { ShowWorkItemDTO } from "../../work-items/models/show-work-item.dto";
import { Expose, Type } from "class-transformer";
import { ShowMembersDTO } from "../../teams/models/show-members.dto.";
import { ShowTeamDTO } from "../../teams/models/show-team.dto";
import { ShowUserDTO } from "../../users/models/show-user.dto";
import { ShowStatusDTO } from "../status/models/show-status.dto";
import { ShowTagDTO } from "../tags/models/show-tag.dto";
import { ShowApprovedRateRevReqDTO } from "../approved-rate/models/show-approved-rate.dto";
import { TeamEntity } from "../../entities/teams.entity";
import { StatusEntity } from "../../entities/status.entity";
import { TagEntity } from "../../entities/tag.entity";
import { ApprovedRateRevReqEntity } from "../../entities/approved-rev-req.entity";
import { CommentEntity } from "../../entities/comments.entity";
import { ShowCommentDTO } from "./show-comment.dto";

export class ShowRevReqDTO {
    // ON TO ONE , must include separately..@Expose()
    // workItem: ShowWorkItemDTO;
    @Expose()
    revReqId: string;

    @Expose()
    creator: ShowUserDTO;

    @Expose()
    //@Type(() => ShowTeamDTO)
    teamOfViewers: ShowTeamDTO  | TeamEntity;

    @Expose()
    reviewers: ShowMembersDTO[]; // todo ?? // ShowUserDTO ??

    @Expose()
    //@Type(() => ShowStatusDTO )
    status: StatusEntity | string; ;//ShowStatusDTO | string; //'Pending' | 'Under Review' | 'Change Requested' | 'Rejected' | 'Approved';
    // TypeError: Converting circular structure to JSON  => over simplify status type
     //ShowStatusDTO; // just the status.type and rev req..
    // | string | object | ShowStatusDTO; // | object

    @Expose()
    comments: ShowCommentDTO[] | CommentEntity[] | string;

    @Expose()
    @Type(() => ShowTagDTO )
    tags: TagEntity[] | string; //; ShowTagDTO[]

    @Expose()
    approvedRateRevReqs: ApprovedRateRevReqEntity[];//ShowApprovedRateRevReqDTO[]; //ApprovedRateRevReqEntity[];//

    @Expose()
    createdOn: Date;
}
