import { IsString } from "class-validator";

export class AddTagDTO {

    @IsString()
    name: string;

}