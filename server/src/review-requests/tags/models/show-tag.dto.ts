import { Expose, Type } from "class-transformer";
import { ShowRevReqDTO } from "../../models/show-rev-req.dto";
import { ReviewRequestEntity } from "../../../entities/review-req.entity";

export class ShowTagDTO {
    @Expose()
    tagId:string;

    @Expose()
    name: string;

    @Expose()
    //@Type(() => ShowRevReqDTO)
    reviewRequest: ReviewRequestEntity[] | string; // ??
    
    @Expose()
    isDeleted: boolean;
}
