import { Expose, Type } from "class-transformer";

export class ShowTagNameDTO {
    @Expose()
    name: string;
}
