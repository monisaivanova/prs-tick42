import { ShowUserDTO } from "../../../users/models/show-user.dto";
import { ShowRevReqDTO } from "../../../review-requests/models/show-rev-req.dto";
import { Expose } from "class-transformer";
import { ReviewRequestEntity } from "../../../entities/review-req.entity";

export class ShowApprovedRateRevReqDTO {
    
    @Expose()
    approvedRateId: string;
  
    @Expose()
    isApproved: boolean;
    @Expose() 
    reviewRequest: ReviewRequestEntity; //ShowRevReqDTO;
    @Expose() 
    reviewer: ShowUserDTO;
}