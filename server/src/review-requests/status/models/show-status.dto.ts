import { ShowRevReqDTO } from "../../models/show-rev-req.dto";
import { Expose } from "class-transformer";
import { ReviewRequestEntity } from "../../../entities/review-req.entity";

export class ShowStatusDTO { 
    @Expose()
    statusId: string;

    @Expose()
    reviewRequests: ShowRevReqDTO[] | string;

    @Expose()
    type: string;

}