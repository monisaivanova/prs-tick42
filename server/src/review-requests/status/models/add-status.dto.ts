import { IsString } from "class-validator";

export class AddStatusDTO {
    @IsString()
    type: string;
}
