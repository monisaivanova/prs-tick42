import { Controller, Put, Post, Param, Body, ValidationPipe, UseGuards, Req, Get } from '@nestjs/common';
import { ReviewRequestsService } from './review-requests.service';
import { ShowRevReqDTO } from './models/show-rev-req.dto';
import { CreateRevReqDTO } from './models/create-rev-req.dto';
import { AuthGuard } from '@nestjs/passport';
import { AddMembersDTO } from '../teams/models/add-members.dto';
import { AddTagDTO } from './tags/models/add-tag.dto';
import { AddStatusDTO } from './status/models/add-status.dto';
import { CreateCommentDTO } from '../review-requests/models/create-comment.dto';
import { ShowCommentDTO } from '../review-requests/models/show-comment.dto';
import { AddApprovedRateRevReqDTO } from './approved-rate/models/add-approved-rate.dto';
import { TitleWorkItemDTO } from '../work-items/models/title.dto';

@UseGuards(AuthGuard())
@Controller('api/work-items/:workItemId/review-requests')
export class ReviewRequestsController {
    constructor( 
        private readonly revReqService: ReviewRequestsService,
    ){} 

    @Get(':revReqId')
    async findOneRevReq(
          @Param('workItemId') workItemId: string,
          @Param('revReqId') revReqId: string): Promise<ShowRevReqDTO> {
      return await this.revReqService.findOneRevReq(workItemId, revReqId);
    }

    

    @Post()
    async createRevReq( 
        @Param('workItemId') workItemId: string,
        @Body(new ValidationPipe({ whitelist: true,
             transform: true })) 
        teamName: CreateRevReqDTO, 
        @Req() request,
        ): Promise<ShowRevReqDTO>{ 
        return await this.revReqService
        .createRevReq( workItemId, teamName, request.user);
    }

    @Put(':revReqId') 
    async addReviewer(// todo // REMOVE reviewer ALSO
        //@Param('workItemId') workItemId: string,
        @Param('revReqId') revReqId: string, //no need, but bera
        @Body(new ValidationPipe({ whitelist: true, transform: true })) reviewerMail: AddMembersDTO,
    ): Promise<ShowRevReqDTO>{
        //selectReviewers//only from the team.members
        return await this.revReqService.addReviewer(revReqId, reviewerMail);
    }

    @Post(':revReqId/comments')
    //addTags
    async addComment(
        @Param('workItemId') workItemId: string,
        @Param('revReqId') revReqId: string,
        @Body(new ValidationPipe({ whitelist:true, transform: true})) 
        message: CreateCommentDTO,
        @Req() request,
    ): Promise<ShowCommentDTO>{
        return await this.revReqService.addComment(workItemId, revReqId, message, request.user );
    }    


    @Put(':revReqId/tags')
    //addTags
    async addTag(
        //@Param('workItemId') workItemId: string, ????
        @Param('revReqId') revReqId: string,
        @Body(new ValidationPipe({ whitelist:true, transform: true})) name: AddTagDTO,
    ): Promise<ShowRevReqDTO>{
        return await this.revReqService.addTag(revReqId, name );
    }    

    //removeTag from tag list
    // todo // optionally... 
    @Put(':revReqId/remove-tags')
    //addTags
    async removeTag(
        @Param('revReqId') revReqId: string,
        @Body(new ValidationPipe({ whitelist:true, transform: true})) name: AddTagDTO,
    ): Promise<ShowRevReqDTO>{
        return await this.revReqService.removeTag(revReqId, name );
    }  

    //directStatusChange
    @Put(':revReqId/status')
    async directStatusChange(
        @Param('workItemId') workItemId: string,
        @Param('revReqId') revReqId: string,
        @Body(new ValidationPipe({ whitelist:true, transform: true})) 
        type: AddStatusDTO,
    ): Promise<ShowRevReqDTO>{
        return await this.revReqService.directStatusChange(workItemId, revReqId, type );
    }  

    //indirectStatusChange
    //  an approval entity needs to be inserted in db
    @Post(':revReqId/status-approve')
    async approveAsIndirectStatusChange(
        @Param('workItemId') workItemId: string,
        @Param('revReqId') revReqId: string,
        @Body(new ValidationPipe({ whitelist:true, transform: true})) 
        isApproved: AddApprovedRateRevReqDTO,
        @Req() request,
    ): Promise<ShowRevReqDTO>{
        return await this.revReqService.approveAsIndirectStatusChange(workItemId, revReqId, isApproved, request.user  );
    }  


    // @Post('notify')
    // async notify( @Param('workItemId') workItemId: string,){
        
    //     return await this.revReqService.notify();
        
    // }

    @Post('notify/rev-req')
    async notifyForRevReq(
        @Param('workItemId') workItemId: string,
        @Body(new ValidationPipe({ whitelist:true, transform: true})) 
        workItemTitle: TitleWorkItemDTO,
    ){
        return await this.revReqService.notifyForRevReq(workItemTitle);
        
    }


}
