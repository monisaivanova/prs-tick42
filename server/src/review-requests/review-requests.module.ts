import { Module } from '@nestjs/common';
import { ReviewRequestsController } from './review-requests.controller';
import { ReviewRequestsService } from './review-requests.service';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WorkItemEntity } from '../entities/work-items.entity';
import { UserEntity } from '../entities/users.entity';
import { ReviewRequestEntity } from '../entities/review-req.entity';
import { TeamEntity } from '../entities/teams.entity';
import { ConverterService } from '../common/converter.service';
import { ApprovedRateRevReqEntity } from '../entities/approved-rev-req.entity';
import { StatusEntity } from '../entities/status.entity';
import { TagEntity } from '../entities/tag.entity';
import { CommentEntity } from '../entities/comments.entity';

@Module({
  imports: [
    PassportModule.register({defaultStrategy: 'jwt'}),
    TypeOrmModule.forFeature([
      TeamEntity,
      WorkItemEntity,
      UserEntity,
      ReviewRequestEntity,
      ApprovedRateRevReqEntity,
      StatusEntity,
      TagEntity,
      CommentEntity
    ]),
  ],
  controllers: [ReviewRequestsController],
  providers: [ReviewRequestsService, ConverterService]
})
export class ReviewRequestsModule {}
