import { ConfigService } from './../config/config.service';
import { ConfigModule } from './../config/config.module';
import { UserEntity } from '../entities/users.entity';
import { UsersModule } from './../users/users.module';
import { CoreModule } from '../core/core.module';
import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { config } from '../common/config';
import { TypeOrmModule } from '@nestjs/typeorm';
//import { UsersService } from '../users/users.service';

@Module({
  imports: [
    CoreModule,
    ConfigModule,
    UsersModule,
    TypeOrmModule.forFeature([UserEntity]),
    PassportModule.register({defaultStrategy: 'jwt'}),
    JwtModule.registerAsync({
      imports: [ConfigModule,],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secretOrPrivateKey: configService.jwtSecret,
        signOptions: {
          expiresIn: configService.jwtExpireTime,
        },
      }),
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
