import { Test, TestingModule } from '@nestjs/testing';
import { WorkItemsService } from './work-items.service';
import { WorkItemsController } from './work-items.controller';

describe('WorkItemsService', () => {
  let service: WorkItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WorkItemsController],
      providers: [WorkItemsService],
    }).compile();

    service = module.get<WorkItemsService>(WorkItemsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
