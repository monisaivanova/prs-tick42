import { Expose } from "class-transformer";
import { ReviewRequestEntity } from "../../entities/review-req.entity";
import { UserEntity } from "../../entities/users.entity";

export class ShowWorkItemDTO {
  @Expose()
  workItemId: string;

  @Expose()
  title: string;

  @Expose()
  description: string;

  @Expose()
  reviewRequest: ReviewRequestEntity; //not a Promise<of it..??>

  @Expose()
  creator: UserEntity | string; // if we just want name..

  @Expose()
  createdOn: Date;

}
