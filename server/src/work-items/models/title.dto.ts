import { IsString, Length } from "class-validator";

export class  TitleWorkItemDTO {
  @IsString()
  @Length(2, 20)
  title: string;

}
