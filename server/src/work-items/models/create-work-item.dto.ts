import { IsString, Length } from "class-validator";

// include mere title + description out of entity properties
// the revReq. will later be added->
// aka the newly created workItem will re-route to
// a path to create revReq, which will then be
// linked to include in its Entity that workItem

export class  CreateWorkItemDTO {
  @IsString()
  @Length(2, 20)
  title: string;

  @IsString()
  @Length(5, 100)
  description: string;
}
