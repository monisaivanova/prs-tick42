import { IsString, Length } from "class-validator";
export class  DescriptionUpdateWorkItemDTO {
  @IsString()
  @Length(5, 100)
  description: string;
}
