import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { WorkItemEntity } from '../entities/work-items.entity';
import { Repository } from 'typeorm';
import { UserEntity } from '../entities/users.entity';
import { ShowWorkItemDTO } from './models/show-work-item.dto';
import { ConverterService } from '../common/converter.service';
import { CreateWorkItemDTO } from './models/create-work-item.dto';
import { ShowTagNameDTO } from '../review-requests/tags/models/show-tag-name.dto';
import { TagEntity } from '../entities/tag.entity';
import { ShowTagDTO } from '../review-requests/tags/models/show-tag.dto';
import { ReviewRequestEntity } from '../entities/review-req.entity';
import { DescriptionUpdateWorkItemDTO } from './models/description-update-work-item.dto';


@Injectable()
export class WorkItemsService {

  //  !NB: a workItem = a COMPONENT/part of a reviewRequest
  // the reviewRequestEntity and Service-Controller
  // will be our MAIN CONCERN,
  // NOT the WorkItem (=just the content...linked to autor)
  // JUST LIKE COMMENTS - workItem  is mere content,
  // a part of the parent component:  reviewRequest
  constructor(
    @InjectRepository(WorkItemEntity)
    private readonly workItemRepository: Repository<WorkItemEntity>,
    @InjectRepository(ReviewRequestEntity)
    private readonly revReqRepository: Repository<ReviewRequestEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(TagEntity)
    private readonly tagRepository: Repository<TagEntity>,
    private readonly converterService: ConverterService,
  ) {}

  // Methods to implement:
  // !NB: comments are NOT added to workItem, but to revReq
  // findWorkItem
  async findOneWorkItem(workItemId: string): Promise<ShowWorkItemDTO> {
    const foundWorkItem = await this.workItemRepository.findOne({
      where: {
        workItemId,
      }
    });

    if(!foundWorkItem) {
      throw new BadRequestException('Whoops! No such Work Item out there..');
    }

    return await this.converterService.convertToShowWorkItemDTO(foundWorkItem);
  }

  async findWorkItemByRevReqId(revReqId: string): Promise<ShowWorkItemDTO> {
    const foundRevReq = await this.revReqRepository.findOne({
      where: {
        revReqId,
      }
    });
    if(!foundRevReq) {
      throw new BadRequestException('Whoops! No such Review Request out there..');
    }
    
    const foundWorkItem = await this.workItemRepository.findOne({
      where: {
        reviewRequest: foundRevReq,
      }
    });

    if(!foundWorkItem) {
      throw new BadRequestException('Whoops! No such Work Item out there..');
    }

    return await this.converterService.convertToShowWorkItemDTO(foundWorkItem);
  }

  // createWorkItem
  async createWorkItem(workItem: CreateWorkItemDTO, creator: UserEntity): Promise<ShowWorkItemDTO> {
    const newWorkItem: WorkItemEntity = this.workItemRepository
      .create(workItem);
      
    newWorkItem.creator = Promise.resolve(creator);
    
    const savedWorkItem = await this.workItemRepository.save(newWorkItem);
    

    return await this.converterService.convertToShowWorkItemDTO(savedWorkItem);
  }
  
  // editWorkItem
  async editWorkItem(workItemId: string,
    newWorkItem: DescriptionUpdateWorkItemDTO, 
    creator: UserEntity): Promise<ShowWorkItemDTO> {
    const workItemToUpdate = await this.workItemRepository.findOne({
      where: {
        workItemId,
      },
    });

    if (!workItemToUpdate) {
      throw new BadRequestException('WorkItem with this ID does not exist.');
    }

    const WorkItemCreator = await workItemToUpdate.creator;
    // update possible only if You === the Creator of the W.I.
    if (WorkItemCreator.userId === creator.userId) {
     // workItemToUpdate.title = newWorkItem.title;//title unupdateable
      workItemToUpdate.description = newWorkItem.description;
    } else {
      throw new BadRequestException('Only the creator of the Work Item can update it.');
    }

    const correspondingRevReqId = await (await workItemToUpdate.reviewRequest).revReqId;
    const correspondingRevReq = await this.revReqRepository.findOne({
      where: {
        revReqId: correspondingRevReqId,
      }
    });

    const updatedWorkItem = await this.workItemRepository.save(workItemToUpdate);

    (await this.revReqRepository).save(correspondingRevReq);
    return this.converterService.convertToShowWorkItemDTO(updatedWorkItem);
  } 

  // findAllWorkItemsOfUser-- no need - you would need all rev-req-s

  // ?deleteWorkItem

  // all tags
      //get all tags 
      async allTags(): Promise<ShowTagDTO[]>{
        const tagsAll = await this.tagRepository.find(
         // ({   where: {   isDeleted: false,   },  }) 
        );
        //console.log(tagsAll); //ok 
        if(!tagsAll) {
            throw new BadRequestException('Whoops! No tags..');
        }     
        return await this.converterService.convertToShowTagDTOArray(tagsAll);
    }
}
