import { Body, Controller, Get, Param, Post, Req, UseGuards, ValidationPipe, Put } from '@nestjs/common';
import { WorkItemsService } from './work-items.service';
import { AuthGuard } from '@nestjs/passport';
import { ShowWorkItemDTO } from './models/show-work-item.dto';
import { CreateWorkItemDTO } from './models/create-work-item.dto';
import { TagEntity } from '../entities/tag.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ReviewRequestsService } from '../review-requests/review-requests.service';
import { ShowTagNameDTO } from '../review-requests/tags/models/show-tag-name.dto';
import { ShowTagDTO } from '../review-requests/tags/models/show-tag.dto';
import { DescriptionUpdateWorkItemDTO } from './models/description-update-work-item.dto';



@UseGuards(AuthGuard())
@Controller('api/work-items') //1word,not_,not -// mariadb
export class WorkItemsController {

    //  !NB: a workItem = a COMPONENT/part of a reviewRequest
  // the reviewRequestEntity and Service-Controller
  // will be our MAIN CONCERN,
  // NOT the WorkItem (=just the content...linked to autor)
  constructor(
    private readonly workItemsService: WorkItemsService,
   
  ) {}

  //find all work items- add method
  @Get('/tags')
  async allTags(): Promise<ShowTagDTO[]> {
    return await this.workItemsService.allTags();
  }

  @Get(':workItemId')
  async findOneWorkItem(
        @Param('workItemId') workItemId: string): Promise<ShowWorkItemDTO> {
    return await this.workItemsService.findOneWorkItem(workItemId);
  }

  @Get(':revReqId/match')
  async findWorkItemByRevReqId(
        @Param('revReqId') revReqId: string): Promise<ShowWorkItemDTO> {
    return await this.workItemsService.findWorkItemByRevReqId(revReqId);
  }

  @Post()
  async createWorkItem(    
      @Body(new ValidationPipe({ whitelist: true, transform: true }))
      workItem: CreateWorkItemDTO,
      @Req() request ): Promise<ShowWorkItemDTO> {
    return await this.workItemsService.createWorkItem(workItem, request.user);
  }

  @Put(':workItemId')
  async editWorkItem(
      @Param('workItemId') workItemId: string,
      @Body(new ValidationPipe({ whitelist: true, transform: true }))
      workItem: DescriptionUpdateWorkItemDTO,
      @Req() request ): Promise<ShowWorkItemDTO> { 
    return await this.workItemsService.editWorkItem(workItemId, workItem, request.user)
  }
  

}
