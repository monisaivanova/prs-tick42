import { Module } from '@nestjs/common';
import { WorkItemsController } from './work-items.controller';
import { WorkItemsService } from './work-items.service';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WorkItemEntity } from '../entities/work-items.entity';
import { UserEntity } from '../entities/users.entity';
import { ReviewRequestEntity } from '../entities/review-req.entity';
import { ConverterService } from '../common/converter.service';
import { ReviewRequestsService } from '../review-requests/review-requests.service';
import { TagEntity } from '../entities/tag.entity';


@Module({
  imports: [
    PassportModule.register({defaultStrategy: 'jwt'}),
    TypeOrmModule.forFeature([WorkItemEntity, UserEntity,TagEntity, ReviewRequestEntity], ),
  ],
  controllers: [WorkItemsController],
  providers: [WorkItemsService, ConverterService],
})
export class WorkItemsModule {}
