import { AuthGuard } from '@nestjs/passport';
import { ShowUserDTO } from './models/show-user.dto';
import {
    Controller,
    Param,
    UseGuards,
    Get,
    Body,
    ValidationPipe,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { ShowRevReqDTO } from '../review-requests/models/show-rev-req.dto';
import { Username } from './models/username.dto';

@UseGuards(AuthGuard())
@Controller('api/users')
export class UsersController {
  constructor(
    private readonly userService: UsersService,
  ) {}
  // register method: in AuthController and both Auth-/UserService
  // log-in method: in AuthContoller and AuthService
  @Get()
  async findAll(): Promise<ShowUserDTO[]> {
    return this.userService.findAllUsers();
  }


  @Get(':userId')
  async findSingleUser(
    @Param('userId') userId: string,
  ): Promise<ShowUserDTO> {
    return this.userService.findSingleUser(userId);
  }

  @Get(':userId/own-rev-reqs')
  async findAllOwnRevReqs(
    @Param('userId') userId: string,
  ): Promise<ShowRevReqDTO[]> {
    return this.userService.findAllOwnRevReqs(userId);
  }

  @Get(':userId/rev-reqs')
  async findAllRevReqsToReview(
    @Param('userId') userId: string,
  ): Promise<ShowRevReqDTO[]> {
    return this.userService.findAllRevReqsToReview(userId);
  }

  
  // @Get('/localStorage')
  // async findUserByUsername(
  //   @Body(new ValidationPipe({ whitelist: true, transform: true })) 
  //   username: Username
  //   ): Promise<ShowUserDTO> {
  //   return this.userService.findUserByUsername(username);
  // }




}
