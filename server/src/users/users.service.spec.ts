import { BadRequestException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { UsersService } from './users.service';
import { UserEntity } from '../entities/users.entity';
import { ShowUserDTO } from './models/show-user.dto';
import { UserRegisterDTO } from '../auth/models/user-register.dto';
import { UserLoginDTO } from '../auth/models/user-login.dto';

describe('UsersService', () => {
  let service: UsersService;

  const usersRepository = {
    find() {},
    findOne() {},
    create() {},
    save() {},
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(UserEntity),
          useValue: usersRepository,
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  describe('findUserByEmail', () => {
    it('should call usersRepository.findOne() once with correct query', async () => {

      // Arrange
      const email = 'hi@hi.com';

      const testShowUserDTO: ShowUserDTO = {
        userId: 'hihi',
        username: 'hello',
        email: 'hi@hi.com',
        fullName: 'Hi Hi',
      };

      const usersRepositoryFindOne = jest.spyOn(usersRepository, 'findOne')
      .mockImplementation(() => Promise.resolve(testShowUserDTO));

      const query = {
        where: {
          email,
          isDeleted: false,
        },
      };

      // Act
      await service.findUserByEmail(email);

      // Assert
      expect(usersRepositoryFindOne).toBeCalledTimes(1);
      expect(usersRepositoryFindOne).toBeCalledWith(query);

      usersRepositoryFindOne.mockRestore();
    });

    it('should return the user with the given email address.', async () => {

      // Arrange
      const email = 'hi@hi.com';

      const testShowUserDTO: ShowUserDTO = {
        userId: 'hihi',
        username: 'hello',
        email: 'hi@hi.com',
        fullName: 'Hi Hi',
      };

      const usersRepositoryFindOne = jest.spyOn(usersRepository, 'findOne')
      .mockImplementation(() => Promise.resolve(testShowUserDTO));

      const query = {
        where: {
          email,
          isDeleted: false,
        },
      };

      // Act
      const foundUser = await service.findUserByEmail(email);

      // Assert
      expect(foundUser).toEqual(testShowUserDTO);

      usersRepositoryFindOne.mockRestore();
    });

    it('should throw if there is no user with such email address', async () => {

      // Arrange
      const email = 'hi@hi.com';

      const testShowUserDTO: ShowUserDTO = {
        userId: 'hihi',
        username: 'hello',
        email: 'hi@hi.com',
        fullName: 'Hi Hi',
      };

      const usersRepositoryFindOne = jest.spyOn(usersRepository, 'findOne')
      .mockImplementation(() => Promise.resolve(null));

      const query = {
        where: {
          email,
          isDeleted: false,
        },
      };

      // Act and Assert
      try {
        await service.findUserByEmail(email);
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
      }

      usersRepositoryFindOne.mockRestore();
    });
  });

  describe('register', () => {
    it('should call usersRepository.create() and usersRepository.save() one time each with the correct user', async () => {

      // Arrange
      const testShowUserDTO: ShowUserDTO = {
        userId: 'hihi',
        username: 'hello',
        email: 'hi@hi.com',
        fullName: 'Hi Hi',
      };

      const testUserRegisterDTO: UserRegisterDTO = {
        username: 'hello',
        email: 'hi@hi.com',
        password: 'password',
        fullName: 'Hi Hi',
      };

      const usersRepositoryCreate = jest.spyOn(usersRepository, 'create')
      .mockImplementation(() => Promise.resolve(testShowUserDTO));

      const usersRepositorySave = jest.spyOn(usersRepository, 'save')
      .mockImplementation(() => Promise.resolve(testShowUserDTO));

      const hash = jest.spyOn(bcrypt, 'hash')
      .mockImplementation(() => Promise.resolve(testUserRegisterDTO.password));

      // Act
      await service.register(testUserRegisterDTO);

      // Assert
      expect(usersRepositoryCreate).toBeCalledTimes(1);
      expect(usersRepositoryCreate).toBeCalledWith(testUserRegisterDTO);
      expect(usersRepositorySave).toBeCalledTimes(1);
      // expect(usersRepositorySave).toBeCalledWith(testUserRegisterDTO);

      usersRepositoryCreate.mockRestore();
      usersRepositorySave.mockRestore();
      hash.mockRestore();
    });

    it('should return the registered user', async () => {

      // Arrange
      const testShowUserDTO: ShowUserDTO = {
        userId: 'hihi',
        username: 'hello',
        email: 'hi@hi.com',
        fullName: 'Hi Hi',
      };

      const testUserRegisterDTO: UserRegisterDTO = {
        username: 'hello',
        email: 'hi@hi.com',
        password: 'password',
        fullName: 'Hi Hi',
      };

      const usersRepositoryCreate = jest.spyOn(usersRepository, 'create')
      .mockImplementation(() => Promise.resolve(testShowUserDTO));

      const usersRepositorySave = jest.spyOn(usersRepository, 'save')
      .mockImplementation(() => Promise.resolve(testShowUserDTO));

      // Act
      const registeredUser = await service.register(testUserRegisterDTO);

      // Assert
      expect(registeredUser).toEqual(testShowUserDTO);

      usersRepositoryCreate.mockRestore();
      usersRepositorySave.mockRestore();
    });
  });

  describe('validateUserPassword', () => {
    it('should call usersRepository.findOne() once with correct query', async () => {

      // Arrange
      const testUserLoginDTO: UserLoginDTO = {
        email: 'hi@hi.com',
        password: 'password',
      };

      const testShowUserDTO: ShowUserDTO = {
        userId: 'hihi',
        username: 'hello',
        email: 'hi@hi.com',
        fullName: 'Hi Hi',
      };

      const usersRepositoryFindOne = jest.spyOn(usersRepository, 'findOne')
      .mockImplementation(() => Promise.resolve(testShowUserDTO));

      const compare = jest.spyOn(bcrypt, 'compare')
      .mockImplementation(() => Promise.resolve(true));

      const query = {
        where: {
          email: testUserLoginDTO.email,
          isDeleted: false,
        },
      };

      // Act
      await service.validateUserPassword(testUserLoginDTO);

      // Assert
      expect(usersRepositoryFindOne).toBeCalledTimes(1);
      expect(usersRepositoryFindOne).toBeCalledWith(query);

      usersRepositoryFindOne.mockRestore();
      compare.mockRestore();
    });
  });
});
