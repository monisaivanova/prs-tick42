import { Expose } from 'class-transformer';
import { ShowTeamDTO } from '../../teams/models/show-team.dto';

export class ShowUserDTO {

  @Expose()
  userId: string;

  @Expose()
  username: string;

  @Expose()
  email: string;

  @Expose()
  fullName: string;

  // @Expose()
  // teams: ShowTeamDTO[]; 
  

}
