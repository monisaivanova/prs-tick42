import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../entities/users.entity';
import { PassportModule } from '@nestjs/passport';
import { ConverterService } from '../common/converter.service';
import { ReviewRequestEntity } from '../entities/review-req.entity';

@Module({
  imports: [
    PassportModule.register({defaultStrategy: 'jwt'}),
    TypeOrmModule.forFeature([UserEntity, ReviewRequestEntity]),
  ],
  controllers: [UsersController],
  providers: [UsersService, ConverterService],
  exports: [UsersService],
})
export class UsersModule {}
