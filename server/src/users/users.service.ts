import { UserEntity } from '../entities/users.entity';
import { UserLoginDTO } from './../auth/models/user-login.dto';
import { Injectable, BadRequestException } from '@nestjs/common';
import { UserRegisterDTO } from '../auth/models/user-register.dto';
import * as bcrypt from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { ShowUserDTO } from './models/show-user.dto';
import { ConverterService } from '../common/converter.service';
import { TeamEntity } from '../entities/teams.entity';
import { ShowTeamDTO } from '../teams/models/show-team.dto';
import { ShowRevReqDTO } from '../review-requests/models/show-rev-req.dto';
import { ReviewRequestEntity } from '../entities/review-req.entity';
import { Username } from './models/username.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly usersRepository: Repository<UserEntity>,
    @InjectRepository(ReviewRequestEntity)
    private readonly revReqRepository: Repository<ReviewRequestEntity>,

    private readonly converterService: ConverterService,
  ) {}

  async register(user: UserRegisterDTO): Promise<ShowUserDTO> {
    const newUser: UserEntity = this.usersRepository.create(user);

    const passwordHash = await bcrypt.hash(user.password, 10);
    newUser.password = passwordHash;

    const savedUser = await this.usersRepository.save(newUser);

    return this.converterService.convertToShowUserDTO(savedUser);

    // return plainToClass(ShowUserDTO, savedUser, { excludeExtraneousValues: true });
  }

  async findUserByEmail(email: string): Promise<ShowUserDTO> | undefined {
    const foundUser = await this.usersRepository.findOne({
      where: {
        email,
        isDeleted: false,
      },
    });

    if (!foundUser) {
      return undefined;
    }

    return this.converterService.convertToShowUserDTO(foundUser);

    // return plainToClass(ShowUserDTO, foundUser, { excludeExtraneousValues: true });
  }

  // async findUserByUsername(username: Username): Promise<ShowUserDTO> | undefined {
  //   const foundUser = await this.usersRepository.findOne({
  //     where: {
  //       username,
  //       isDeleted: false,
  //     },
  //   });

  //   if (!foundUser) {
  //     return undefined;
  //   }

  //   return this.converterService.convertToShowUserDTO(foundUser);

  //   // return plainToClass(ShowUserDTO, foundUser, { excludeExtraneousValues: true });
  // }


  async validateUserPassword(user: UserLoginDTO): Promise<boolean> {
    const userEntity = await this.usersRepository.findOne({
      where: {
        email: user.email,
        isDeleted: false,
      },
    });

    return await bcrypt.compare(user.password, userEntity.password);
  }

  async findAllUsers(): Promise<ShowUserDTO[]>{
    const userEntities:UserEntity[] = await this.usersRepository.find({
      where: {
        isDeleted: false,
      },
    });
    return this.converterService.convertToShowUserDTOArray(userEntities);
  }

  async findSingleUser(userId: string): Promise<ShowUserDTO>{
    const userEntity: UserEntity = await this.usersRepository.findOne({
      where: {
        userId,
        isDeleted: false,
      },
    });
    if (!userEntity) {
      throw new BadRequestException('User with such ID does not exist.');
    }
    return this.converterService.convertToShowUserDTO(userEntity);
  }

  async findAllOwnRevReqs(userId: string): Promise<ShowRevReqDTO[]> {
    const userEntity: UserEntity = await this.usersRepository.findOne({
      where: {
        userId: userId,
        isDeleted: false,
      },
    });
    if (!userEntity) {
      throw new BadRequestException('User with such ID does not exist.');
    }

    const revReqs = await this.revReqRepository.find({
      where: {
        creator: userEntity,
      }
    })

    //const revReqs = await userEntity.ownRevReqs; // ??    

    return Promise.all(revReqs.map(async (revReq: ReviewRequestEntity) => this.converterService.convertToShowRevReqDTO(revReq)));
  }

  async findAllRevReqsToReview(userId: string, ): Promise<ShowRevReqDTO[]> {
    const userEntity: UserEntity = await this.usersRepository.findOne({
      where: {
        userId: userId,
        isDeleted: false,
      },
    });
    if (!userEntity) {
      throw new BadRequestException('User with such ID does not exist.');
    }

    const revReqs = await userEntity.reviewRequests;

    return Promise.all(revReqs.map(async (revReq: ReviewRequestEntity) => this.converterService.convertToShowRevReqDTO(revReq)));
  }
}
