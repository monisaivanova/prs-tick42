export const config = {
  jwtSecret: 'yourSecretKeyHere',
  expiresIn: 3600*60*24*7,
};
