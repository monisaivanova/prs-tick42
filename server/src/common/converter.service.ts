import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CommentEntity } from '../entities/comments.entity';
import { UserEntity } from '../entities/users.entity';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { TeamEntity } from '../entities/teams.entity';
import { ShowTeamDTO } from '../teams/models/show-team.dto';
import { WorkItemEntity } from '../entities/work-items.entity';
import { ShowWorkItemDTO } from '../work-items/models/show-work-item.dto';
import { ReviewRequestEntity } from '../entities/review-req.entity';
import { ShowRevReqDTO } from '../review-requests/models/show-rev-req.dto';
import { TagEntity } from '../entities/tag.entity';
import { ShowTagNameDTO } from '../review-requests/tags/models/show-tag-name.dto';
import { ShowTagDTO } from '../review-requests/tags/models/show-tag.dto';
import { ShowCommentDTO } from '../review-requests/models/show-comment.dto';

@Injectable()
export class ConverterService {
  constructor(

  ) {}

  async convertToShowUserDTO(user: UserEntity): Promise<ShowUserDTO> {
    const convertedUser: ShowUserDTO = {
      userId: user.userId,
      username: user.username,
      email: user.email,
      fullName: user.fullName,
    };
    return convertedUser;
  }
  async convertToShowUserDTOArray(users: UserEntity[]): Promise<ShowUserDTO[]> {
      return Promise.all(users.map(async (entity: UserEntity) => this.convertToShowUserDTO(entity)));
  }

  async convertToShowTagDTO(tag: TagEntity): Promise<ShowTagDTO> {
    const convertedTag: ShowTagDTO = {
      tagId: tag.tagId,
      name: tag.name,
      reviewRequest: await tag.reviewRequest,
      isDeleted: tag.isDeleted,
    };
    return convertedTag;
  }

  async convertToShowTagDTOArray(tags: TagEntity[]): Promise<ShowTagDTO[]> {
    return Promise.all(tags.map(async (entity: TagEntity) => this.convertToShowTagDTO(entity)));
  }

  async convertToShowTeamDTO(team: TeamEntity): Promise<ShowTeamDTO> {
    const convertedTeam: ShowTeamDTO = {
      teamId: team.teamId,
      teamName: team.teamName,
      owner: await team.owner,
      members: await team.members,
      reviewRequestsToView: await team.reviewRequestsToView,
    };
    return convertedTeam;
  }

  async convertToShowWorkItemDTO(workItem: WorkItemEntity): Promise<ShowWorkItemDTO> {
    const convertedWorkItem: ShowWorkItemDTO = {
      workItemId: workItem.workItemId,
      title: workItem.title,
      description: workItem.description,
      reviewRequest: await workItem.reviewRequest,
      creator: await workItem.creator,
      createdOn: workItem.createdOn,
    }
    return convertedWorkItem;
  }

  async convertToShowRevReqDTO(revReq: ReviewRequestEntity): Promise<ShowRevReqDTO> {
    const convertedRevReq: ShowRevReqDTO = {
      revReqId: revReq.revReqId,
      creator: await revReq.creator,
      teamOfViewers: await revReq.teamOfViewers,
      reviewers: await revReq.reviewers, //1st time: empty
      status: await revReq.status,
      comments: await revReq.comments, // 1st empty
      tags: await revReq.tags, //1st empty
      approvedRateRevReqs: await revReq.approvedRateRevReqs,
      createdOn: new Date(),
    }
 
    return convertedRevReq;
  }

  async convertToShowCommentDTO(comment: CommentEntity): Promise<ShowCommentDTO> {
    const convertedComment: ShowCommentDTO = {
      commentId: comment.commentId,
      message: await comment.message,
      reviewRequest: await comment.reviewRequest,
      reviewer: await comment.reviewer,
    }

    return convertedComment;
  }

}
