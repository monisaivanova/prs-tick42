import { Entity, PrimaryGeneratedColumn, OneToMany, OneToOne, ManyToMany, JoinTable, ManyToOne, CreateDateColumn } from "typeorm";
import { CommentEntity } from "./comments.entity";
import { WorkItemEntity } from "./work-items.entity";
import { UserEntity } from "./users.entity";
import { TagEntity } from "./tag.entity";
import { StatusEntity } from "./status.entity";
import { ApprovedRateRevReqEntity } from "./approved-rev-req.entity";
import { TeamEntity } from "./teams.entity";

@Entity('review_request')
export class ReviewRequestEntity {

  @PrimaryGeneratedColumn('uuid')
  revReqId: string;

  //many to one -creator
  @ManyToOne(type => UserEntity, creator => creator.ownRevReqs)
  creator: Promise<UserEntity>;
  
  // @OneToOne(type => WorkItemEntity, workItem => workItem.reviewRequest)
  // workItem: Promise<WorkItemEntity>;
  // ^^ get its workItem.creator to find revReq author

  @ManyToOne(type => TeamEntity, teamOfViewers => teamOfViewers.reviewRequestsToView)
  teamOfViewers: Promise<TeamEntity>;

  @ManyToMany(type => UserEntity, reviewers => reviewers.reviewRequests)
  @JoinTable()
  reviewers: Promise<UserEntity[]>; // will add service logic to only include team members as possible reviewers

  @OneToMany(type => CommentEntity, comments => comments.reviewRequest)
  // many comments can simultaneously be linked to One(1) revReq
  // comments should include instructions by reviewers to be acted upon by workItem owner
  comments: Promise<CommentEntity[]>;

  @ManyToMany(type => TagEntity, tags => tags.reviewRequest)
  tags: Promise<TagEntity[]>;

  // should be linked to the rew. request
  @ManyToOne(type => StatusEntity, status => status.reviewRequests)
  status: Promise<StatusEntity>;  // !! if not string => circular error  https://makandracards.com/makandra/28847-dealing-with-typeerror-converting-circular-structure-to-json-on-javascript
  // can switch among them// different types be hard-coded or CRUD-ed by admins
  // ^^ will use status.type and will change it, but will always have just 1 status at a time; cannot have 2 simultaneously;
  // server-logic: analyze actions of all reviewers.length about the STATUS change
  
  @OneToMany(type => ApprovedRateRevReqEntity, approvedRateRevReqs => approvedRateRevReqs.reviewRequest)
  approvedRateRevReqs: Promise<ApprovedRateRevReqEntity[]>;
  // similar to likes functionality
  //if all reviewers' approvedRateRevReq.isApproved === true => isApproved

  // perhaps simplify by just: isApproved: boolean ??

  @CreateDateColumn()
  createdOn: Date;
}
