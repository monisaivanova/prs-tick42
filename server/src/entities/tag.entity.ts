import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { WorkItemEntity } from './work-items.entity';
import { ReviewRequestEntity } from './review-req.entity';

@Entity('tags')
export class TagEntity {

  @PrimaryGeneratedColumn('uuid')
  tagId: string;

  @Column()
  name: string;  // different tag names will be added / hard-coded as table rows

  @ManyToMany(type => ReviewRequestEntity, reviewRequest => reviewRequest.tags)
  @JoinTable()
  reviewRequest: Promise<ReviewRequestEntity[]> | string;
  // added "| string;" to prevent TypeError: Converting circular structure to JSON


  @Column({default: false})
  isDeleted: boolean;

}
