import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { ReviewRequestEntity } from './review-req.entity';
import { UserEntity } from "./users.entity";

@Entity('approved-rate-review-requests')
export class ApprovedRateRevReqEntity {
  // ONE reviewer can only ONCE Accept a review request, not multiple times
  @PrimaryGeneratedColumn('uuid')
  approvedRateId: string;

  @Column({default: false})
  isApproved: boolean;

  @ManyToOne(type => ReviewRequestEntity, reviewRequest => reviewRequest.approvedRateRevReqs)
  reviewRequest: Promise<ReviewRequestEntity> | string;

  @ManyToOne(type => UserEntity, reviewer => reviewer.approvedRateRevReqs)
  reviewer: Promise<UserEntity>;
  // action ACCEPT a review request ONLY open to a REVIEWER of that request
  // when all reviewers have 'accept'-ed => CHANGE IN STATUS of revReq

}