import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, ManyToOne, OneToMany } from 'typeorm';
import { UserEntity } from './users.entity';
import { WorkItemEntity } from './work-items.entity';
import { ReviewRequestEntity } from './review-req.entity';

@Entity('teams')
export class TeamEntity {
  @PrimaryGeneratedColumn('uuid')
  teamId: string;

  @Column('nvarchar', {unique: true})
  teamName: string;

  @ManyToOne(type => UserEntity, owner => owner.createdTeams)
  owner: Promise<UserEntity>;

  // TEAMS are only linked to USERS !!!
  @ManyToMany(type => UserEntity, members => members.teams)
  @JoinTable()
  members: Promise<UserEntity[]>;

  @OneToMany(type => ReviewRequestEntity, reviewRequestsToView => reviewRequestsToView.teamOfViewers )
  reviewRequestsToView: Promise<ReviewRequestEntity[]>;
 // no link needed b/n review & team ? -well, not really..
}
