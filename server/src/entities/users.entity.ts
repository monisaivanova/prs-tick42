
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  JoinTable,
  ManyToOne,
} from 'typeorm';
import { TeamEntity } from './teams.entity';
import { WorkItemEntity } from './work-items.entity';
import { CommentEntity } from './comments.entity';
import { ReviewRequestEntity } from './review-req.entity';
import { ApprovedRateRevReqEntity } from './approved-rev-req.entity';

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  userId: string;

  @Column('nvarchar', {unique: true})
  username: string;

  @Column('nvarchar', {unique: true})
  email: string;

  @Column('nvarchar')
  password: string;

  @Column('nvarchar')
  fullName: string;

  @Column({default: false})
  isDeleted: boolean;

  @CreateDateColumn()
  createdOn: Date;

  // TEAM is ONLY linked to users
  @ManyToMany(type => TeamEntity, teams => teams.members)
  teams: Promise<TeamEntity[]>;

  @OneToMany(type => TeamEntity, createdTeams => createdTeams.owner)
  createdTeams: Promise<TeamEntity[]>;

  @OneToMany(type => WorkItemEntity, workItems => workItems.creator)
  workItems: Promise<WorkItemEntity[]>;
  // ^^ aka OWN reviewRequests; accessible property through workItem entity
  // would NOT create a workItem until step2(Create Rev.Req.) is done => create both, @once

  @OneToMany(type => ReviewRequestEntity, ownRevReqs => ownRevReqs.creator)
  ownRevReqs: Promise<ReviewRequestEntity[]>;

  @ManyToMany(type => ReviewRequestEntity, reviewRequests => reviewRequests.reviewers )
  reviewRequests: Promise<ReviewRequestEntity[]>;
  // where assigned as reviewer ^^^

  @OneToMany(type => CommentEntity, comments => comments.reviewer)
  comments: Promise<CommentEntity[]>;

  @OneToMany(type => ApprovedRateRevReqEntity, approvedRateRevReqs => approvedRateRevReqs.reviewer)
  approvedRateRevReqs: Promise<ApprovedRateRevReqEntity[]>;

  // role - alex- add role property to user entity- 1 user, 1 team, 1 role
}
