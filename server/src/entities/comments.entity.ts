import { Entity, ManyToOne, PrimaryGeneratedColumn, Column } from "typeorm";
import { UserEntity } from "./users.entity";
import { ReviewRequestEntity } from "./review-req.entity";


@Entity('comments')
export class CommentEntity {
  @PrimaryGeneratedColumn('uuid')
  commentId: string;

  @Column('nvarchar')
  message: string;

  @ManyToOne(type => ReviewRequestEntity, reviewRequest => reviewRequest.comments)
  reviewRequest: Promise<ReviewRequestEntity>;

  @ManyToOne(type => UserEntity, reviewer => reviewer.comments)
  reviewer: Promise<UserEntity>;
}
