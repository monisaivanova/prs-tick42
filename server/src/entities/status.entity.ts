import { PrimaryGeneratedColumn, ManyToMany, Column, Entity, OneToMany } from "typeorm";
import { WorkItemEntity } from "./work-items.entity";
import { ReviewRequestEntity } from "./review-req.entity";

@Entity('statuses')
export class StatusEntity {
  // !! ONLY FOR ADMINs
  @PrimaryGeneratedColumn('uuid')
  statusId: string;

  @OneToMany(type => ReviewRequestEntity, 
    reviewRequests => reviewRequests.status,
//{cascade: true}
    )
  //not visible in table of statuses
   reviewRequests: Promise<ReviewRequestEntity[]> | string; 


  @Column('nvarchar', 
    //{default: 'Pending'}
  )
  type: string;
  // Once the work item is submitted for review it could be in one of the following statuses:
  // (to be added as db table-rows)
  // 1) Pending, // DEFAULT value// b4 ANY interaction(comment/like/etc), less mere view(not a valid interaction)
  // 2) Under Review, // after ANY interaction(comment/like/etc) // DIRECT STATUS CHANGE
  // 3) Change Requested, // after A PARTICULAR interaction BY ANY of the reviewers of the revReq // DIRECT STATUS CHANGE
  // 4) Accepted, // after A PARTICULAR interaction BY ALL of the REVIEWERS of the revReq //like Likes // front-end logic// the stars from Heros
  // 5) Rejected. // after A PARTICULAR interaction BY ANY of the reviewers of the revReq // DIRECT STATUS CHANGE

  // an administrator, let's say, may add via a command, new types
  // of statuses, as new rows of the status table, thus relacing the ENUM functionality;
  // AVOID using enum on db , aka Entity level !!!
}
