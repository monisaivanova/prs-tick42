import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany, JoinTable, OneToOne, CreateDateColumn, JoinColumn } from 'typeorm';
import { TeamEntity } from './teams.entity';
import { UserEntity } from './users.entity';
import { CommentEntity } from './comments.entity';
import { StatusEntity } from './status.entity';
import { TagEntity } from './tag.entity';
import { ReviewRequestEntity } from './review-req.entity';

@Entity('work_items')
export class WorkItemEntity {
  @PrimaryGeneratedColumn('uuid')
  workItemId: string;

  @Column('nvarchar')
  title: string;

  @Column('nvarchar')
  description: string;

  @OneToOne(type => ReviewRequestEntity)//, reviewRequest => reviewRequest.workItem)
  @JoinColumn()
  reviewRequest: Promise<ReviewRequestEntity>;

  @ManyToOne(type => UserEntity, creator => creator.workItems)
  creator: Promise<UserEntity>; // === creator

  // should we need that? // NO !! a work-item shall NOT be directly linked with a TEAM
  // teams are only linked to users !!
  // no!  @ManyToOne(type => TeamEntity, team => team.workItemEntities)
  // no! team: Promise<TeamEntity>;

  @CreateDateColumn()
  createdOn: Date;

}
