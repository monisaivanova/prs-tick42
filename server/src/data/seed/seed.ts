import { CommentEntity } from './../../entities/comments.entity';
import { WorkItemEntity } from '../../entities/work-items.entity';
import { UserEntity } from '../../entities/users.entity';
import { createConnection } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { StatusEntity } from '../../entities/status.entity';
import { TagEntity } from '../../entities/tag.entity';
import { TeamEntity } from '../../entities/teams.entity';
import { ReviewRequestEntity } from '../../entities/review-req.entity';

const main = async () => {
  const connection = await createConnection();

  const userRepository = connection.manager.getRepository(UserEntity);
  const statusRepository = connection.manager.getRepository(StatusEntity);
  const tagRepository = connection.manager.getRepository(TagEntity);
  const teamRepository = connection.manager.getRepository(TeamEntity);
  const revReqRepository = connection.manager.getRepository(ReviewRequestEntity);
  const workItemRepository = connection.manager.getRepository(WorkItemEntity);
  // The seed script starts here:
  // seed for the TAGS list
  // reviewRequests will be added/ removed later to each status entry==type

  const tagBug = new TagEntity();
  tagBug.name = 'bug report';
  tagBug.isDeleted = false;
  await tagRepository.save(tagBug);

  const tagFeature = new TagEntity();
  tagFeature.name = 'feature request';
  tagFeature.isDeleted = false;
  await tagRepository.save(tagFeature);

  const tagTechnical = new TagEntity();
  tagTechnical.name = 'documentation issue';
  tagTechnical.isDeleted = false;
  await tagRepository.save(tagTechnical);

  const tagOrganisational = new TagEntity();
  tagOrganisational.name = 'organisational question';
  tagOrganisational.isDeleted = false;
  await tagRepository.save(tagOrganisational);

  const tagImportant = new TagEntity();
  tagImportant.name = 'important';
  tagImportant.isDeleted = false;
  await tagRepository.save(tagImportant);
  console.log('Tags created');

  // seed for the STATUSes list
  // reviewRequests will be added/ removed later to each status entry==type
  //const statusDefault = new StatusEntity();
  const statusDefault = await statusRepository.create(); //todo
  statusDefault.type = 'Pending';
  await statusRepository.save(statusDefault);

  const statusAfterFirstInteraction = new StatusEntity();
  statusAfterFirstInteraction.type = 'Under Review';
  await statusRepository.save(statusAfterFirstInteraction);

  const statusChange = new StatusEntity();
  statusChange.type = 'Change Requested';
  await statusRepository.save(statusChange);

  const statusApproved = new StatusEntity();
  statusApproved.type = 'Approved';
  await statusRepository.save(statusApproved);

  const statusRejected = new StatusEntity();
  statusRejected.type = 'Rejected';
  await statusRepository.save(statusRejected);
  console.log('Statuses created');

  // seed for users
  const Sharo = await userRepository.findOne({
    where: {
      username: 'Sharo',
    },
  });

  if (!Sharo) {
    const user0 = new UserEntity();
    user0.username = 'sharo';
    user0.email = 'sharo@gmail.com'; // later: REAL e-mails!!!
    user0.password = await bcrypt.hash('password', 10);
    user0.fullName = 'Sharo Tuck';
    user0.createdOn = new Date();

    await userRepository.save(user0);
    console.log('sharo created!');
  } else {
    console.log('sharo is already in the Database!');
  }

  const u = await userRepository.findOne({
    where: {
      username: 'u',
    },
  });

  if (!u) {
    const user00 = new UserEntity();
    user00.username = 'u';
    user00.email = 'u@u.com'; // later: REAL e-mails!!!
    user00.password = await bcrypt.hash('password', 10);
    user00.fullName = 'Uuuu Uuuuu';
    user00.createdOn = new Date();

    await userRepository.save(user00);
    console.log('u created!');
  } else {
    console.log('u is already in the Database!');
  }

  const Josephine = await userRepository.findOne({
    where: {
      username: 'josephine',
    },
  });

  if (!Josephine) {
    const user1 = new UserEntity();
    user1.username = 'josephine';
    user1.email = 'j@j.com'; // later: REAL e-mails!!!
    user1.password = await bcrypt.hash('password', 10);
    user1.fullName = 'Josephine Jacobs';
    user1.createdOn = new Date();

    await userRepository.save(user1);
    console.log('Josephine created!');
  } else {
    console.log('Josephine is already in the Database!');
  }

  const Esmeralda = await userRepository.findOne({
    where: {
      username: 'esmi92',
    },
  });

  if (!Esmeralda) {
    const user2 = new UserEntity();
    user2.username = 'esmi92';
    user2.email = 'e@e.com'; // later: REAL e-mails!!!
    user2.password = await bcrypt.hash('password', 10);
    user2.fullName = 'Esmeralda Ivanova';
    user2.createdOn = new Date();

    await userRepository.save(user2);
    console.log('Esmeralda created!');
  } else {
    console.log('Esmeralda is already in the Database!');
  }

  //seed for work items // todo
  // never trust the seed file !!
//   const wi  = await workItemRepository.findOne({
//     where: {
//       title: 'work item of i',
//     },
//   });
//   if (!wi) {
//   const workItemI = new WorkItemEntity();
//   workItemI.title = 'work item of i';
//   workItemI.description = 'Life is what you make sure you turn off the iron, in case you ever use it , and we know you don\'t, so take this as a kind message from the Universe.';
//   workItemI.creator = Promise.resolve(await userRepository.findOne({where: {username: 'i'}}));
//   workItemI.reviewRequest = Promise.resolve(await revReqRepository
//     .findOne({where: { creator: workItemI.creator}}) // ??
//   );
//   await workItemRepository.save(workItemI);
//   console.log('workItemI created!');
//   } else {
//     console.log('workItemI is already in the Database!');
//   }

  //seed for teams
  const t  = await teamRepository.findOne({
    where: {
      teamName: 'team sharo',
    },
  });
  if (!t) {
  const teamI = new TeamEntity();
  teamI.teamName = 'team sharo';
  teamI.owner = Promise.resolve(await userRepository.findOne({where: {username: 'sharo'}}));
  teamI.members = Promise.resolve(await userRepository.find(
    {
      where: [
      {
        username: 'sharo'
      },
      {
        username: 'u'
      },
      {
        username: 'josephine'
      }
    ]
  }));
  // teamI.reviewRequestsToView = Promise.resolve(await revReqRepository.find(
  //   {
  //     where: [{},{}] // todo
  //   }
  // ))
  await teamRepository.save(teamI);
  console.log('team sharo was created');
  } else {
    console.log('team sharo is already in the Database!');
  }

  
//   //seed for review requests // todo
//   // const r  = await revReqRepository.findOne({
//   //   where: {
//   //      : 'team of i',
//   //   },
//   // });
//   // if (!r) {
//   const revReqI = new ReviewRequestEntity();
//   revReqI.creator =  Promise.resolve(await userRepository.findOne({
//     where: {
//       username: 'i',
//     }
//   }) );
//   revReqI.teamOfViewers = Promise.resolve(await t); //??

//  // revReqI.comments
//   //revReqI.tags 
//   //TODO // problem with seed file //
//   // TypeError: Cannot read property 'findOne' of undefined
//   //status  - was NOT assigned to default = 'Pending'//because statuses are still not defined!!
//   revReqI.status = Promise.resolve(await this.statusRepository.findOne({
//     where: {
//         type: statusDefault.type,
//     }
//   }));
//   // approvedRateRevReqs

//   await revReqRepository.save(revReqI);
//   //assign team to a 'defined'
//   revReqI.reviewers = Promise.resolve(await this.userRepository.find(
//     {
//       where: [
//         {
//           username: 'u'
//         }
//       ]
//     }
//   ));
//   //link to work item:
//   wi.reviewRequest = Promise.resolve(revReqI);
//   await workItemRepository.save(wi);




 


  connection.close();
}

main().catch(console.error);
